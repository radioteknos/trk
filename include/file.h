/*
 * TRK - Satellite tracking program based on Norad SGP/SDP model with
 *       curses interface
 *
 *	by Lapo Pieri IK5NAX  2000-2020
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *  Send bugs reports, comments, critique, etc, to ik5nax@radioteknos.it
 */

/* file templates */

const char obs_template[]="\
# THIS IS A TEMPLATE FOR obs.dat ADJUST TO YOUR NEEDS\n\
##\n\
## Position of the observers\n\
##\n\
#\n\
# The position [Home] must always be present, the others are optional.\n\
#\n\
# The identifier of the observer (the name) must be enclosed in [].\n\
#\n\
# The angles are in degrees (positive for north and east), the height in\n\
# meters, and the QTHlos must have 6 characters.\n\
#\n\
# If only the QTHloc is provided, the position will be assumed to be in the\n\
# center of that square.\n\
#\n\
# It is possible to only specify lat,lon, or only the QTH; if both are\n\
# specified, they must be compatible, otherwise the observer will not\n\
# be accepted.\n\
#\n\
# If the height is not specified, it will be set to zero.\n\
\n\
# Main station\n\
[Home]\n\
lon	11.2180161\n\
lat	43.7778742\n\
height	69\n\
QTHloc	JN53OS\n\
Descr	IK5NAX\n\
Horiz\n\
0	4\n\
30	3\n\
45	2\n\
60	3\n\
90	2\n\
120	2\n\
180	2\n\
270	1\n\
Horiz_end\n\
\n\
# Baikonur cosmodrome\n\
[Baikonur]\n\
lon	63.3\n\
lat	45.9\n\
height	0\n\
Descr	Baikonur\n\
\n\
# Korou\n\
[Korou]\n\
lon	307.3\n\
lat	5.2\n\
height	0\n\
Descr	ESA French Guiana launch site\n\
\n\
\n\
# End of obs.dat\n\
";

const char tle_template[]="\
AO-7\n\
1 07530U 74089B   20198.38798810 -.00000037  00000-0  51953-4 0  9995\n\
2 07530 101.8012 167.9693 0011953 310.2360 204.6161 12.53644215089612\n\
";

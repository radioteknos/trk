/*
 * TRK - Satellite tracking program based on Norad SGP/SDP model with
 *       curses interface
 *
 *	by Lapo Pieri IK5NAX  2000-2020
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *  Send bugs reports, comments, critique, etc, to ik5nax@radioteknos.it
 */

#include "sgp.h"

/*============================================================================
                           Definition and Constant
 ============================================================================*/
#define R        6378.135
#define	J2000	2451545.0

/* Earth's reference geoid (WGS ???) */
#define aT       6378.140
#define bT       6356.755  
#define eT       0.08181922


#define mu       3.986012e5  /* Km^3/s^2 */

/* Comment the following line if system dosnn't have X11, check Makefile too */
#define X11


#define Xmap_OBSADD 1
#define Xmap_OBSDEL 2
#define Xmap_SATSET 3
#define Xmap_SATDEL 4

/*============================================================================
                                   Structures
 ============================================================================*/
/* Human Readable Time structure */
struct hr_time {
	int	y;	/* year */
	int	mo;	/* month */
	int	dn;	/* day of the month */
	int	dw;	/* day of the week */
	int	dy;	/* day of the week */
	int	h;	/* hour */
	int	mi;	/* minute */
	int	s;	/* seconds */
	int	ms;	/* milliseconds */
	};

struct satdb {
  char name[40];          /* Conventional name */
  char group[2];          /* Group (still unused)*/
  long norad;             /* NORAD designation (without classification) */ 
  float beacon[8];        /* Beacon frequency (MHz) */
  float uplink[8];        /* Uplink central frequency (MHz) */
  float downlink[8];      /* Downlink central frequency (MHz) */
  char matrix[8][7][256]; /* The first index is the IF matrix description
			     or the operating mode dependent on day of the week
			     (second index) and/or phase (third index) */
  float alon,alat;        /* attitude; alon=-1000 if unknown */
  struct satdb *next;     /* pointer to next satdb structure */
};

struct gc_time{
	int	tzme;	/* timezone in minut east of Greenwich */
};

typedef struct satposition{
  char name[40];               /* Conventional name (tle line 0)     */
  long norad;                  /* Norad catalog number               */
  char class;                  /* Norad classification (always U)    */
  double x;                    /* ECI coordinate, rewritten here,    */
  double y;                    /* BUT different for unit of measure  */
  double z;                    /* from that in SATS  (here in Km)    */
  double az;                   /* Azimuth                            */
  double el;                   /* Elevation                          */
  double range;                /* Distance                           */
  double rangerate;            /* Modulus of velocity respect to the 
				  observer                            */
  double phase;                /* True Anomaly in 256 fraction        */
  double ra;                   /* Rigth ascension                     */
  double dec;                  /* Declination                         */
  double ssplon;               /* Longitude of sub satellite point    */
  double ssplat;               /* Latitude of sub satellite point     */
  double height;               /* Quote from reference geoid          */
  long revn;                   /* Orbit number                        */
  double squint;               /* Squint angle (-1 if not available)  */
} satpos;

typedef struct observer{
  char name[16];       /* Conventional name */
  char descr[70];      /* Description */

  struct observer *prev;   /* linked list */
  struct observer *next;   /* linked list */

  float lat;           /* Geodetical latitude positiv north [rad] */
  float lon;           /* Geodetical longitude positiv east [rad] */
  float h0;            /* Height above sea level [km]             */
  char qth[6];         /* QTH locator 6 character [upper case] */
  float horiz[2][72];  /* Local horizon, az,elmin, (one point each
			  5 degree max)                           */
}OBS;

typedef struct configuration_file{
  char obsdatf[80];            /* FIlename observers                      */ 
  char tlef[80];               /* FIlename tle                            */
  char satdbf[80];             /* FIlename satellit database              */
  char gelf[80];               /* FIlename gpt command elevation plot     */
  char gelfX[80];              /* FIlename gpt command elevation plot (X) */
  char gpttf[80];              /* FIlename gpt presence check             */
  char schedfmt[80];           /* Filename sched format                   */
  char fiforot[80];            /* FIlename rotord (or Yd) command         */
  char fifoxmap[80];           /* FIlename trkXmap command                */
  char fifodop[80];            /* doppler output fifo name                */
}cfg_file;


/*============================================================================
                            Function prototypes
 ============================================================================*/

/* FIle association is WRONG ! */

/* in trk2.c */
void single_sat_calc(double, SAT *, OBS *, satpos *), 
  graphel(SAT *, OBS *obs, int);
void sub_sat_point(double, double, double, double, double *, double *,
		   double *);
double elev(double, SAT *, OBS *), getT(char *);
void rotor(float, float), help(void), obsinfo(OBS *),
  derivtle(double, SAT *, int);
void doppler(float rr, float el);
SAT *getsatname(SAT *);
void read_db(FILE *,SAT *);
struct satdb *search_db(SAT *);
void sched(SAT *, OBS *, double, float, float), 
  passov(SAT *, OBS *, double, float),
  multisat(double, SAT *, OBS *, int, int, int);
char sat_illum(satpos, satpos);
void xmap(int, float, float, float, char *);

/* in qthloc.c */
int lonlat2qthloc(float, float, char*), qthloc2lonlat(char*, float*, float*);


/* in coord.c */
void ijk2sez(double, double, double, double, double, 
	     double *, double *, double *);
void sez2ijk(double, double, double, double, double, 
	     double *, double *, double *);
void sez2azel(double , double , double ,
	      double *, double *, double *);
void radec2azel(double, double, double, double,
		double *, double *);
void elaz2radec(double, double, double, double,
		double *, double *);
void ijk2radec(double, double, double, double *, double *);
void radec2ijk(double, double, double *, double *, double *);


/* in sunmoon.c */
void sunpos(double, OBS *, satpos *);
void moonpos(double, OBS *, satpos *);


/* in t2.c */
void UTCstring(double, char *, int), jd2hr(double, struct hr_time *),
     DTstring(double, char *, int, int, int, int, double);
double JDsyst(void), tetha(double, float), hr2jd(struct hr_time *);
char *hms(double);


/* in obs.c */
void obsijk(double, OBS *, float *, float *, float *);
OBS *getobs(int);
double localhoriz(OBS *, double, int);


/* in predict.c */
int AOS(SAT *, OBS *, double, double *), 
  LOS(SAT *, OBS *, double, double *);


/* in file.c */
int setup_config_file(int, char **, cfg_file *);


/* in calc.c */
void min3of4(double *, double *, double *, double *, double *, double *,
	     double , double);



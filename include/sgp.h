/*
 * TRK - Satellite tracking program based on Norad SGP/SDP model with
 *       curses interface
 *
 *	by Lapo Pieri IK5NAX  2000-2020
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *  Send bugs reports, comments, critique, etc, to ik5nax@radioteknos.it
 */

/*
Originally from:
 *  COLA -- a satellite close-approach finder
 *  Copyright (C) 1996 Matthew Francey
 */

#ifndef	SGP_H
#define	SGP_H
#include <stdio.h>
#include <time.h>

#ifdef	__cplusplus
extern "C" {
#endif

/*
 * a satellite
 */
typedef	struct sat {
	int	type;		/* kind of object   */
	struct	sat	*prev;	/* linked list      */
	struct	sat	*next;	/* linked list      */
	char	*name;		/* name             */
	double	 t;		/* time             */
	double	 x[3];		/* position         */
	double	dx[3];		/* velocity         */
	void	*mdata;		/* model constants  */
	void	*idata;		/* initialized data */
	unsigned long mode;	/* status bits      */

	/*
	 * line 0
	 */
	double	len, wid, hgt;	/* size of the object                   */
	double	mag0;		/* absolute magnitude                   */

	/*
	 * line 1 of the element set
	 */
	long	norad;		/* NORAD catalogue number               */
	char	eclass;		/* NORAD "class" field (use unknown)    */
	char	ephtyp;		/* NORAD "ephemeris type" (use unknown) */
	char	desig[10];	/* international launch designation     */
	double	t0;		/* epoch                                */
	double	dn0;		/* (1/2)*d(n0)/dt                       */
	double	ddn0;		/* (1/6)*d^2(d0)/(dt^2)                 */
	double	bstar;		/* drag parameter                       */
	int	bulnum;		/* bulletin number                      */

	/*
	 * line 2 of the element set
	 */
	double	incl0;		/* inclination                         */
	double	node0;		/* right ascensio of ascending node    */
	double	ecc0;		/* eccentricity                        */
	double	peri0;		/* argument of perigee                 */
	double	M0;		/* mean anomaly                        */
	double	n0;		/* mean motion                         */
	long	rev0;		/* rev number                          */

	/*
	 * what all the models compute
	 */
	long	rev;
	double	semi, incl, node, peri, ecc, mean, r, dr, rdu, u, n;
	double	N[3], M[3];	/* orientation unit vectors            */
	double	U[3], V[3];	/* position and velocity unit vectors  */
} SAT;

/*
 * bitmap for ->mode
 */
#define	SGP_SAT_CHECKSUM1	0x00000001L	/* checksum error, line 1 */
#define	SGP_SAT_CHECKSUM2	0x00000002L	/* checksum error, line 2 */
#define	SGP_SAT_SIZEINFO	0x00000004L	/* len/wid/hgt/mag0       */
#define	SGP_SAT_HASNAME		0x00000008L	/* has given name         */

/*
 * support functions
 */
extern	SAT	*sgp_sat_read(FILE *);
extern	int	 sgp_sat_write(FILE *, SAT *);
extern	SAT	*sgp_sat_free(SAT *);
extern	void	 sgp_sat_list_free(SAT *);

/*
 * model functions
 */
extern	SAT	*sgp(SAT *s, double t);
extern	SAT	*sgp4(SAT *s, double t);

/*
 * a ground position
 */
typedef struct site {
	int	type;			/* kind of object   */
	struct	site	*prev;		/* linked list      */
	struct	site	*next;		/* linked list      */
	char	*name;			/* name of site     */
	double	 t;			/* time             */
	double	 x[3];			/* position         */
	double	dx[3];			/* velocity         */
	void	*mdata;			/* model constants  */
	void	*idata;			/* initialized data */
	unsigned long mode;		/* status bits      */

	/*
	 * stuff loaded
	 */
	int	datum;		/* geodetic datum            */
	double	lat;		/* latitude (positive north) */
	double	lon;		/* longitude (positive east) */
	double	alt;		/* altitude (metres)         */

	/*
	 * pre-computed on load
	 */
	double	gx, gz;
	double	cos_lat;
	double	sin_lat;

	/*
	 * orientation
	 */
	double	n[3];		/* the direction of north  */
	double	e[3];		/*                  east   */
	double	z[3];		/*                  zenith */
} SITE;

/*
 * the whole earth -- construction and destruction
 */
typedef struct {
	double	e2;		/* ecc^2                          */
	double	f;		/* flattening                     */
	double	r;		/* eq. radius                     */
	double	a;		/* height of atmosphere (optical) */
	double	ar;		/* height of atmosphere (reentry) */
	double	w;		/* rotation rate                  */
} EARTH;

/*
 * support functions
 */
extern	SITE	*sgp_site_read(FILE *);
extern	int	 sgp_site_write(FILE *, SITE *);
extern	SITE	*sgp_site_create(char *, double, double, double);
extern	SITE	*sgp_site_free(SITE *);
extern	void	 sgp_site_list_free(SITE *);

/*
 * model function
 */
extern	SITE	*sgp_site(SITE *, double);
extern	EARTH	*sgp_earth(void);
extern	void	 sgp_earth_free(EARTH *);

/*
 * time functions
 */
extern	long	sgp_century;
extern	long	sgp_dnow;
extern	double	sgp_date(double t);
extern	double	sgp_now(void);
extern	double	sgp_getdate(char *p);
extern	double	sgp_ymd(double t);
extern	double	sgp_ydd(double t);
extern	double	sgp_gmst(double t);
extern	time_t	sgp_unix(double);

/*
 * direction towards the Sun
 */
extern	double	*sgp_sun(double t);

#ifdef __cplusplus
};
#endif
#endif

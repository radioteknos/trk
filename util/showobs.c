/*
 * TRK - Satellite tracking program based on Norad SGP/SDP model with
 *       curses interface
 *
 *	by Lapo Pieri IK5NAX  2000-2001
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *  Send bugs reports, comments, critique, etc, to ik5nax@amsat.org
 */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

int lonlat2qthloc(float, float, char*), qthloc2lonlat(char*, float*, float*);

typedef struct observer{
  char name[16];       /* Convenzional name */
  char descr[70];      /* Description */

  struct observer *prev;   /* linked list */
  struct observer *next;   /* linked list */

  float lat;           /* Geodetical Latitude positive north [rad] */
  float lon;           /* Geodetical Longitude positive east [rad] */
  float h0;            /* Height above sea level [Km] */
  char qth[6];         /* QTH locator 6 character [upper case] */
}OBS;

int printobs(OBS *obs){
    
  while(obs!=NULL){
    printf("Obs: %s\n", obs->name);
    printf("\"%s\"\n", obs->descr);
    printf("Lat %f\tLon %f\tAlt %f\tQTH %s\n", obs->lat, obs->lon, obs->h0, 
	   obs->qth);
    obs=obs->next;
    printf("\n");
  }
  
  printf("\n");
  return 0;
}
 

int main(int argc, char **argv){
  FILE *fpobs;
  char lb[80], b[80];
  int n, e=1, chk;
  OBS *obs, *h, *t;
  float v, lon, lat;

  if(argc<2){
    printf("\nshowobs <observerdatabasefiel>\n");
    printf("\n Show database observer (generally called obs.dat)\n\n");
    return 0;
  }

  if((fpobs=fopen(argv[1], "r"))==NULL) {
    fprintf(stderr, "Unable to open %s\n", argv[1]); return 1;
  }

  obs=NULL; h=NULL; t=NULL;

  while(e){
    for(n=0; n<80; n++) lb[n]=0;

    /* Check for end of file */
    if(fgets(lb, 78, fpobs)==NULL) { e=0; lb[0]='['; }

    /* character "#" is for comment */
    if(lb[0]=='#') continue;

    /* If "[" has been found ... */
    if(lb[0]=='[') {          
      if(obs!=NULL) {              /* ... but it isn't the first observer */
                                   /* 
				      than the obs structure can be saved 
				      in the linked list:it's not completely
				      clear to me but it's similar to sgp.c
				      from COLA and it works...
				   */

	/* First of all do some check and fill unspecified field */

	chk=0;
	/* If lon and lat hasn't been read, than try to obtain from QTH */
	if(obs->lat>90.&&obs->lon>180.){
	  if(obs->qth[0]==0) chk=1;                   /* No lon, lat, neither
							 qthloc */
	  else if(qthloc2lonlat(obs->qth, &lon, &lat)<0) chk=2; /* Error on
								   format of
								   QTHloc */
	  else { obs->lon=lon; obs->lat=lat; }
	}

	/* Otherwise check if lon,lat agree with QTH */
	else if(obs->qth[0]!=0) {
	  lonlat2qthloc(obs->lon, obs->lat, b);
	  if(strcmp(b, obs->qth)!=0) chk=3;
	}

	/* If QTH isn't in data must be calculated */
	else{
	  lonlat2qthloc(obs->lon, obs->lat, b);
	  strcpy(obs->qth, b);
	}

	/* Some other check could be done, e.g. if only lon or only lat
	   are present, but this seems to be enough */
	
	/* If data are correct, save in the linked list */
	if(chk==0){
	  if(h==NULL)
	    h=obs;
	  if(t!=NULL)
	    t->next=obs;
	  obs->prev=t;
	  t=obs;
	}
      
	/* If, otherwise, data aren't correct no need to do anything, except
	   for error signaling, if request */
	else{
	  switch(chk){
	  case 1:
	    fprintf(stderr, "No lon, lat, neither QTHloc for [%s]!\n",
		    obs->name); break;
	  case 2:  
	    fprintf(stderr, "Error in QTHloc format for [%s]\n", obs->name);
	    break;
	  case 3:
	    qthloc2lonlat(obs->qth, &lon, &lat);
	    fprintf(stderr, "QTHloc do not match for [%s]: should be %s or \
lon %7.3f, lat %7.3f\n", obs->name, b, lon, lat);
	    break;

	  default: printf("Unknown error!\n"); break;
	  }
	
	}
      }

      /* Now memory can be reserved for a new structure; with linnked list
	 method is not necessary to free memory with free(obs) */
      obs=(OBS *)malloc(sizeof(OBS));
      
      /* Unvalid or null data are imposed to check what has been assigned
	 before to save the structure */
      obs->lat=91.;
      obs->lon=181.;
      obs->h0=0.;      
      obs->qth[0]=0;
      obs->descr[0]=0;
      obs->name[0]=0;

      n=1;
      if(e!=0){
      while(lb[n]!=']') { obs->name[n-1]=lb[n]; n++; }
      obs->name[n-1]=0;
      }
}

    
    /* If otherwise the row don't begin with a "#", neither a "[", than
       data must be analyzed */
    else{      

      /* String should be trasformed to upper */
      if(strncmp(lb, "lon", strlen("lon"))==0){ 
  	if(sscanf(lb+strlen("lon")+1, "%f", &v)!=EOF)
	  obs->lon=v;	
	else 
	  obs->lon=181.;
      }
      if(strncmp(lb, "lat", strlen("lat"))==0){ 
  	if(sscanf(lb+strlen("lat")+1, "%f", &v)!=EOF)
	  obs->lat=v;	
	else
	  obs->lat=91.;
      }
      if(strncmp(lb, "height", strlen("height"))==0){ 
  	if(sscanf(lb+strlen("height")+1, "%f", &v)!=EOF)
	  obs->h0=v;
	else
	  obs->h0=0.;
      }
      if(strncmp(lb, "QTHloc", strlen("QTHloc"))==0){ 
  	if(sscanf(lb+strlen("QTHloc")+1, "%s", b)!=EOF)
	  strcpy(obs->qth, b);
	else
	  obs->qth[0]=0;
      }
      /* I don't like this string elaboration, there are no check on length,
	 is assumed that a single character (0x0d) will terminate the row; 
	 could be done better, even if it work */
      if(strncmp(lb, "Descr", strlen("Descr"))==0){ 
  	strncpy(obs->descr, lb+strlen("Descr")+1,
		strlen(lb+strlen("Descr")+1)-1);
	obs->descr[strlen(lb+strlen("Descr")+1)-1]=0;
      }

      /* If the field is not recognized, it is simply ignored, could be done 
	 better... */
    }
  }

  /* The begin of linked list is h */
  printobs(h);

  fclose(fpobs);
  return 0;
}

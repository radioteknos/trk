/*
 * TRK - Satellite tracking program based on Norad SGP/SDP model with
 *       curses interface
 *
 *	by Lapo Pieri IK5NAX  2000-2020
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *  Send bugs reports, comments, critique, etc, to ik5nax@radioteknos.it
 */

#include <stdio.h>
#include <unistd.h>
#include <errno.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <sys/stat.h>
#include "main.h"
#include "file.h"

/* predefined names of file/fifo */
/* TODO: revise all */
#define TRKDIR     ".trk/"
#define TLEDAT     "trk.tle"
#define OBSDAT     "obs.dat"
#define GEGPT_CMD  "graphel.cmd"
#define GEGPTX_CMD "graphelX.cmd"
#define SATDB      "sat.db"
#define GPT_CMD    "testgpt.cmd"
#define SCHED_FMT  "sched.fmt"
#define FIFOROT    "trkOUTrot"
#define FIFOXMAP   "trkOUTxmap"
#define FIFODOP    "trkOUTdoppler"


/* search for path and filename and store it in the cfg_file structure */
int setup_config_file(int argc, char **argv, cfg_file *cfgf){

  int mode;
  char homedir[40], cfgdir[40], gf[80];
  FILE *fp;
  
  /* search for .trk dir in $(HOME); if not present create it and signal */
  mode=F_OK;
  strcpy(homedir, getenv("HOME"));
  if(homedir==NULL) {
    printf("Unable to find HOME dir ?!\n");
    return 1;
  }
  strcat(homedir, "/");
  strcpy(cfgdir, homedir);
  strcat(cfgdir, TRKDIR);
  if(access(cfgdir, mode)==-1) {
    if(mkdir(cfgdir, 0755)==-1){
      printf("Unable to find a \".trk\" dir in $(HOME) and impossible to \
create\n");
      return 1;
    }
  }

  /* if get here the name of dir containing configuration file is in cfgdir;
     next must be verified that at least obs.dat and trk.tle are present here, 
     or, if the last is missing that a tle filename will be fournished in the 
     command line (-t option) */
  mode=R_OK;
  strcpy(gf, cfgdir);
  strcat(gf, OBSDAT);
  if(access(gf, mode)==-1){
    if((fp=fopen(gf, "w"))==NULL){
      printf("%s not found and unable to write to %s\n", OBSDAT, cfgdir);
      return 1;
    }
    if(fputs(obs_template, fp)==EOF){
      printf("unable to write %s in %s\n", OBSDAT, cfgdir);
      return 1;
    }
    fclose(fp);
  }
  strcpy(cfgf->obsdatf, gf);

  /* check if a tle filename is given in the command line or use default */
  if(cfgf->tlef[0]==0) {
    strcpy(cfgf->tlef, cfgdir);
    strcat(cfgf->tlef, TLEDAT);
  }
  mode=R_OK; 
  if(access(cfgf->tlef, mode)==-1) {      
    fprintf(stderr,
	    "\nUnable to open tle file \"%s\",\nsupplying a template: don't use it \n",
	    cfgf->tlef);
    sleep(3);
    if((fp=fopen(cfgf->tlef, "w"))==NULL){
      printf("%s not found and unable to write to %s\n", cfgf->tlef, cfgdir);
      return 1;
    }
    
    if(fputs(tle_template, fp)==EOF){
      printf("unable to write %s in %s\n", cfgf->tlef, cfgdir);
      return 1;
    }
    fclose(fp);
  }

  /* the following file are created if not present and if for the given cfgdir
     there is the write permission */
  
  /* Satellites database */
  mode=R_OK;
  strcpy(gf, cfgdir);
  strcat(gf, SATDB);
  if(access(gf, mode)==-1){
    if((fp=fopen(gf, "w"))==NULL){
      printf("%s not found and unable to write to %s\n", SATDB, cfgdir);
      return 1;
    }
    fprintf(fp, "# sat.db\n#\n");
    fprintf(fp,
	    "# Skeleton generated automatically by trk, update manually\n");
    fprintf(fp, "#\n# Use \"#\" as comment character\n");
    fprintf(fp,
	    "# Each satellite is specified by its norad number between []\n");
    fprintf(fp,
	    "# Follow some field beginning with a string and one or more \
values\n");
    fprintf(fp, "# The only field used in present version is \"Beacon\",\
 followed by a float\n");
    fprintf(fp, "# Example:\n# [00001]\n# Beacon 20.005\n");
    fclose(fp); 
  }
  strcpy(cfgf->satdbf, gf); 
  

 /* gnuplot command for plotting of elevation */
  mode=R_OK;
  strcpy(gf, cfgdir);
  strcat(gf, GEGPT_CMD);
  if(access(gf, mode)==-1){
    if((fp=fopen(gf, "w"))==NULL){
      printf("%s not fund and unable to write to %s\n", GEGPT_CMD, cfgdir);
      return 1;
    }
    fprintf(fp, "set xdata time\nset format x \"%%d/%%m %%H:%%M\"\n");
    fprintf(fp, "set timefmt \"%%d/%%m/%%Y %%H:%%M:%%S\"\n");
    fprintf(fp, "set zeroaxis\nset yrange [-90:90]\n");
    fprintf(fp, "plot 'el.dat' using 1:3 with lines\n");
    
    fclose(fp);
 }
  strcpy(cfgf->gelf, gf);
  
  /* same for gnuplot under X terminal */
  mode=R_OK;
  strcpy(gf, cfgdir);
  strcat(gf, GEGPTX_CMD);
  if(access(gf, mode)==-1){
    if((fp=fopen(gf, "w"))==NULL){
      printf("%s not fund and unable to write to %s\n", GEGPT_CMD, cfgdir);
      return 1;
    }
    fprintf(fp, "set xdata time\nset format x \"%%d/%%m %%H:%%M\"\n");
    fprintf(fp, "set timefmt \"%%d/%%m/%%Y %%H:%%M:%%S\"\n");
    fprintf(fp, "set zeroaxis\nset yrange [-90:90]\n");
    fprintf(fp, "plot 'el.dat' using 1:3 with lines\n");
    fprintf(fp, "pause -1\n");
    
    fclose(fp);
  }
  strcpy(cfgf->gelfX, gf);
  

  /* Commands for gnuplot presence checking */
  mode=R_OK;
  strcpy(gf, cfgdir);
  strcat(gf, GPT_CMD);
  if(access(gf, mode)==-1){
    if((fp=fopen(gf, "w"))==NULL){
      printf("%s not fund and unable to write to %s\n", GPT_CMD, cfgdir);
      return 1;
    }

    fprintf(fp, "print \"GNUPLOT\"\n");

    fclose(fp);
  }
  strcpy(cfgf->gpttf, gf);
  
  
  /* schedule format */
  mode=R_OK;
  strcpy(gf, cfgdir);
  strcat(gf, SCHED_FMT);
  if(access(gf, mode)==-1){
    if((fp=fopen(gf, "w"))==NULL){
      printf("%s not fund and unable to write to %s\n", SCHED_FMT, cfgdir);
      return 1;
    }
    fprintf(fp, "datez timez az el range ill\n");
    fclose(fp);
  }
  strcpy(cfgf->schedfmt, gf);


  mode=F_OK;
  strcpy(gf, cfgdir);
  strcat(gf, FIFOROT);
  if(access(gf, mode)==-1){
    if(mkfifo(gf, 0600)==-1){
      printf("%s not found and unable to create it in %s\n", FIFOROT, cfgdir);
      return 1;
    }
  }
  strcpy(cfgf->fiforot, gf);
  
  mode=F_OK;
  strcpy(gf, cfgdir);
  strcat(gf, FIFOXMAP);
  if(access(gf, mode)==-1){
    if(mkfifo(gf, 0600)==-1){
      printf("%s not found and unable to create it in %s\n", FIFOXMAP, cfgdir);
      return 1;
    }
  }
  strcpy(cfgf->fifoxmap, gf);


  mode=F_OK;
  strcpy(gf, cfgdir);
  strcat(gf, FIFODOP);
  if(access(gf, mode)==-1){
    if(mkfifo(gf, 0600)==-1){
      printf("%s not found and unable to create it in %s\n", FIFODOP, cfgdir);
      return 1;
    }
  }
  strcpy(cfgf->fifodop, gf);
  
  /* TODO: remove? */ 
  fprintf(stderr, "%s\n%s\n%s\n%s\n%s\n%s\n%s\n", 
	  cfgdir, cfgf->obsdatf, cfgf->tlef, cfgf->satdbf, cfgf->gelf, 
	  cfgf->gpttf, cfgf->fiforot);
  
  return 0;
}

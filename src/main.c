/*
 * TRK - Satellite tracking program based on Norad SGP/SDP model with
 *       curses interface
 *
 *	by Lapo Pieri IK5NAX  2000-2020
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *  Send bugs reports, comments, critique, etc, to ik5nax@radioteknos.it
 */

#define _XOPEN_SOURCE /* glibc2 needs this */
#include <stdio.h>
#include <ncurses.h>
#include <errno.h>
#include <unistd.h>
#include <fcntl.h>
#include <math.h>
#include <string.h>
#include <strings.h>
#include <stdlib.h>
#include <time.h>
#include <ctype.h>
#include <sys/time.h>
#include <sys/stat.h>
#include <X11/Xlib.h>
#include "sgp.h"
#include "main.h"

#define __VERSION "v1.0"

int fdpoint, ai=1, isGPT=0, actobs=0, Zloc=1, fdxmap, fddop;

cfg_file cfgf;

SAT *satlist=NULL;


struct satdb sdb;
struct satdb *satdbs=NULL;
struct satdb satdb_empty;


int main(int argc, char **argv){
  FILE *fp, *fpdb;
  SAT *sats[6], *localsat; 
  OBS *obs[6], imdobs;
  int clo, e=1, roten=0, i, isFREEZ=0, horiz0[6]={0, 0, 0, 0, 0, 0},
    multien=0, isSCHED=0, isPASSOV=0, isROT=0, gptfifochk, isX=0, Xen=0,
    isFT=0, fastdir=1, chrestart, offset=0, numsat, n, dopen=0, isDOP=0;
  
  int numobs;          /* total number of observer (0 for "Home" only),
			  and number of active observer */

  char b[160], b1[30], imdqth[7], initsatname[40], initobs[40], sqs[10];
  
  satpos *asp, *sunp, *moonp;
  float imdlon=-999., imdlat=-999.;
  /* 
     Variables starting with "imd" are about 
     on-the-fly introduction (immediate) of a new
     observer
  */
  
  double T, tLOS, tAOS, tstartsched, tforsched, tstepsched;

#ifdef X11
  Display *Xdisplay;
#endif

 cfgf.tlef[0]=0;
 initsatname[0]=0;
 initobs[0]=0;
 numobs=0;
 actobs=0;
 numsat=0;

 /* Command line scan */
 if(argc>1) {
   while(1){
     clo=getopt(argc, argv, "ht:vrs:o:O:k:p:");
     if(clo==-1) break;
     switch(clo){
     case 't':
       if(sscanf(optarg, "%s", cfgf.tlef)==0 ){
	 printf("Invalid tle filename\n");
	 return 1;
       }
       break; 
     case '?':
     case 'h':
       fprintf(stdout, "\nUsage:\n\n");
       fprintf(stdout, "-h (this) help\n");
       fprintf(stdout, "-v print version number\n");
       fprintf(stdout, "-t <tlefile> specify two line elements file\n");
       fprintf(stdout, "-s <satname> select a satellite by name or norad nuber\
\n");
       fprintf(stdout, "-r start with rotord enabled on selected sat\n");
       fprintf(stdout, "-k print schedule for selected sat (read man)\n");
       fprintf(stdout, "   <from> time or \"now\", <for> hour, <step> minutes\
\n");
       fprintf(stdout, "-o <obsname> start with obsname observer\n"); 
       fprintf(stdout, "-O <[lon lat] || [qthloc]> supply Home observer\
 location\n"); 
       fprintf(stdout, "-p print passages overview (read man)\n"); 
       fprintf(stdout, "\n");
       return 0;
     case 'v':
       fprintf(stdout, "trk version %s\n", __VERSION);
       return 0;
     case 'r':
       roten=1;
       break;
     case 's':
       if(sscanf(optarg, "%s", initsatname)==0 ){
	 printf("Invalid satellite name\n");
	 return 1;
       } 
       for(i=0; i<strlen(initsatname); i++) 
	 initsatname[i]=toupper(initsatname[i]);
       break; 
     case 'o':
       if(sscanf(optarg, "%s", initobs)==0 ){
	 printf("Invalid observer name\n");
	 return 1;
       } 
       for(i=0; i<strlen(initobs); i++) 
	 initobs[i]=toupper(initobs[i]);
       break;
     case 'O':
       strncpy(initobs, optarg, 37);
       imdlon=1.;
       imdlat=1.;
       break;

     case 'k':
       strcpy(b, optarg);
       isSCHED=1;
       break;
     case 'p': strcpy(b, optarg);
       isPASSOV=1;
       break;
     }
   }
 }
 
asp=(satpos*)malloc(sizeof(satpos));    /* This strucuture hold data for
					   the actual pointing of a satellite,
					   that is the common use structure
					   for all */
sunp=(satpos*)malloc(sizeof(satpos));   /* Same for the Sun */
moonp=(satpos*)malloc(sizeof(satpos));  /* And for the Moon */


/* Search for configurations and data files and fifo */
if(setup_config_file(argc, argv, &cfgf)==1) return 1;

/* Open and read of needed data file */
 if((fp=fopen(cfgf.tlef, "r"))==NULL) { 
   fprintf(stderr, "Unable to open tle data file %s\n", cfgf.tlef);
   return 1;
 }
 if((satlist=sgp_sat_read(fp))== NULL) {
   fprintf(stderr, "No objects?\n");
   return 1;
 }
 fclose(fp);
 sats[0]=satlist;

 if((obs[0]=getobs(1))==NULL && imdlon<0. && imdlat<0.){
   fprintf(stderr, "Unable to open obs.dat\n");
   return 1;
 }
 while(obs[0]!=NULL){
   if(strcmp(obs[0]->name, "Home")==0) break;
   obs[0]=obs[0]->next;
 }
 if(obs[0]==NULL && imdlon<0. && imdlat<0.){
   fprintf(stderr, "No \"Home\" observer found in obs.dat\n");
   return 1;
 }

 while(sats[0]->prev!=NULL)
   sats[0]=sats[0]->prev;
 while(sats[0]->next!=NULL){
   numsat++;
   sats[0]=sats[0]->next; }
 while(sats[0]->prev!=NULL)
   sats[0]=sats[0]->prev;

 if(initobs[0]!=0){
   if(imdlon<0. && imdlat<0.) {
     obs[1]=getobs(0);
     while(obs[1]!=NULL){
       if(strncasecmp(initobs, obs[1]->name, strlen(initobs))==0) break;
       obs[1]=obs[1]->next;
     }
     if(obs[1]!=NULL) { 
       numobs=1;
       actobs=numobs; 
       sats[actobs]=satlist;
     }
   }
   else {
     /* Command line supplied lon, lat for home observer */
     strncpy(b, initobs, 38);
     if((b[0]<='9' && b[0]>='0')||b[0]=='-'||b[0]=='+'){ 
       sscanf(b, "%f %f", &imdlon, &imdlat);
       if(lonlat2qthloc(imdlon, imdlat, imdqth)<0) {
	 fprintf(stderr, "Invalid lon, lat for command line supplied Home\
 observer\n");
	 return 1;
       }
       else {
	 obs[0]->lon=imdlon/180.*M_PI;
	 obs[0]->lat=imdlat/180.*M_PI; 
	 strcpy(obs[0]->qth, imdqth);
	 obs[0]->h0=0.;
	 strcpy(obs[0]->descr, "Command line supplied Home observer as lon,lat"
		);
	 strcpy(obs[0]->name, "Home");
       }
     }
     else{
       sscanf(b, "%s", imdqth);
       if(qthloc2lonlat(imdqth, &imdlon, &imdlat)<0) { 
	 fprintf(stderr, "Wrong QTH for command line supplied Home location\n"
		 );
	 return 1;
       }
       else {
	 obs[0]->lon=imdlon/180.*M_PI;
	 obs[0]->lat=imdlat/180.*M_PI; 
	 imdqth[6]=0;
	 obs[0]->h0=0.;
	 strcpy(obs[0]->name, "Home");
	 strcpy(obs[0]->qth, imdqth);
	 strcpy(obs[0]->descr,"Command line supplied Home observer as QTHloc");
       }
     }
     
   }
 }

 if((fpdb=fopen(cfgf.satdbf, "r"))==NULL){ 
   fprintf(stderr, "Unable to open satellite database file %s\n", 
	   cfgf.satdbf);
   return 1;
 }
 read_db(fpdb,satlist);
 fclose(fpdb);


 if(initsatname[0]!=0) {
   while(sats[0]!=NULL) {
     if(strstr(sats[0]->name, initsatname)!=NULL) break;
     sats[0]=sats[0]->next;
   }
   if(sats[0]==NULL) {
     fprintf(stderr, "Unable to find satellite \"%s\"\n", initsatname);
     return 1;
   }
 }
 

 /* If a schedule has been requested, from command line argouments, 
    do the calculation required and exit */
 if(isSCHED==1){
   for(i=0; i<strlen(b); i++) if(b[i]==',') break;
   strncpy(b1, b, i);
   b1[i]=0;
   tstartsched=getT(b1);
   if(sscanf(b+i+1, "%lf,%lf", &tforsched, &tstepsched)==0){
     printf("Invalid schedule data\n");
     return 1;
   }   
   DTstring(tstartsched, b, 1, 1, 1, 0, T);
   sched(sats[actobs], obs[actobs], tstartsched, tforsched, tstepsched);
   return 0;
 }

 /* If a passage overview has been requested, from command line argouments,
    do the calculation required and exit */
 if(isPASSOV==1){
   for(i=0; i<strlen(b); i++) if(b[i]==',') break;
   strncpy(b1, b, i);
   b1[i]=0;
   tstartsched=getT(b1);
   if(sscanf(b+i+1, "%lf", &tforsched)==0){
     printf("Invalid schedule data\n");
     return 1;
   }   
   DTstring(tstartsched, b, 1, 1, 1, 0, 0);

   printf("%lf %f\n", tstartsched, tforsched);
   passov(sats[actobs], obs[actobs], tstartsched, tforsched);

   return 0;
 }


 /* ncurses startup */
 initscr();
 cbreak();
 noecho();
 timeout(0);
 start_color();
 curs_set(0);
 init_pair(1, COLOR_GREEN, COLOR_BLACK);
 init_pair(2, COLOR_YELLOW, COLOR_BLACK);
 init_pair(3, COLOR_WHITE, COLOR_BLACK);
 init_pair(4, COLOR_BLACK, COLOR_CYAN);
 init_pair(5, COLOR_RED, COLOR_BLACK); 
 init_pair(6, COLOR_CYAN, COLOR_BLACK);


 /* Permanent writings on main screen */
 attron(COLOR_PAIR(5)|A_BOLD);
 mvprintw(23,0,"TRK   %s", __VERSION);
 attroff(A_BOLD);
 mvprintw(23, 15, "[ "); 




 /* External services check */ 
 /* Fifo toward rotor */
 if((fdpoint=open(cfgf.fiforot, O_RDWR|O_NONBLOCK))!=-1)
   {
     attron(COLOR_PAIR(5)|A_BOLD); 
     printw("R "); 
     attroff(A_BOLD);
     isROT=1;
   }
 if(isROT==0) roten=0;                /* If rotor is not present ensure
					 it is disabled, since it could be
					 enabled by command line */

 if(roten==1) { 
   attron(COLOR_PAIR(5));
   attron(A_BOLD);
   mvprintw(3, 12, "R"); 
 }


 /* Gnuplot */
 /* This seems to me the only way to get the result of system()...it work */ 
remove("gpttst.fifo");
 if((mkfifo("gpttst.fifo", S_IRUSR|S_IWUSR)!=0)||
    (gptfifochk=open("gpttst.fifo", O_RDWR|O_NONBLOCK))==0)
   fprintf(stderr, "Unable to check if Gnuplot is present\n");
 else{
   sprintf(b, "gnuplot %s >/dev/null 2>./gpttst.fifo", cfgf.gpttf);
   system(b);
   read(gptfifochk, b, 40);
   if(strncmp("GNUPLOT", b, strlen("GNUPLOT"))==0){
     attron(COLOR_PAIR(5)|A_BOLD); 
     printw("G "); 
     attroff(A_BOLD);
     isGPT=1;
   }
 }     
 remove("gpttst.fifo");

#ifdef X11
 /* Test for X presence */
 if((fdxmap=open(cfgf.fifoxmap, O_RDWR|O_NONBLOCK))!=-1) {   
   if((Xdisplay=XOpenDisplay(""))!=NULL){
     attron(COLOR_PAIR(5)|A_BOLD); 
     printw("X "); 
     attroff(A_BOLD);
     isX=1;
     XCloseDisplay(Xdisplay);
   }
 }
#endif


 /* doppler (rangerate) output */
 if((fddop=open(cfgf.fifodop, O_RDWR|O_NONBLOCK))!=-1)
   {
     attron(COLOR_PAIR(5)|A_BOLD); 
     printw("D "); 
     attroff(A_BOLD);
     isDOP=1;
   }
 if(isDOP==0) dopen=0;                /* If doppler is not present ensure
					 it is disabled, since it could be
					 enabled by command line (TODO) */

 if(dopen==1) { 
   attron(COLOR_PAIR(5));
   attron(A_BOLD);
   mvprintw(3, 14, "D"); 
 }
 /* TODO */
 
 printw("]");
 /* End of external services check */

 /* Main loop */
 while(e) {

   /* First of all get the time for all calculus: it can be obtained from
      system clock, or can be a fast incresed time for visual prediction
      mainly with trkXmap; refer to JDsyst() to know what kind of time T is. */
   if(isFT){
     T+=fastdir*60./86400.;
   }
   else
     T=JDsyst();

   /* Seems the only place where to stop the loop if the visualization of 
      a fixed instant is required; I think it could be do better... */
   if(isFREEZ==1) { 
     timeout(-1);
     chrestart=getch(); if(chrestart!=' ') ungetch(chrestart);
     timeout(0);
     isFREEZ=0;
   }

   if((n=getch())!=ERR){
     switch(n){
     case 'q':
       if(Xen==1)	{
	 xmap(Xmap_SATDEL, 0, 0, 0, sats[actobs]->name);
	 xmap(Xmap_SATDEL, 0, 0, 0, sats[actobs]->name);
       }
       e=0;
       break;
       
     case 's':
       sched(sats[actobs], obs[actobs], 0, 0, 0);
       break;
       
     case 'g':
       if(isGPT==1)
	 graphel(sats[actobs], obs[actobs], isX);
       break;
     case 'r':
       if(isROT==1) roten^=0x01;
       if(roten==1) { 
	 attron(COLOR_PAIR(5)); attron(A_BOLD); mvprintw(3, 12, "R"); }
       else
	 mvprintw(3, 12, "  ");
       break;
     case ' ':
       timeout(-1); 
       DTstring(T, b, 1, 1, Zloc, 0, T);
       attroff(A_BOLD);
       attron(COLOR_PAIR(3)|A_REVERSE);
       mvprintw(0,50,"%s", b);
       chrestart=getch();
       timeout(0);
       if(chrestart!=' ') ungetch(chrestart); 
       attroff(A_REVERSE);
       isFREEZ=0; 
       if (isFREEZ==1) attroff(A_REVERSE);
       break;
       
     case 'z':
       Zloc^=0x01; break;
     case 'h': 
     case '?':
       help();
       break;
     case 'a':
       ai^=0x01;
       break;
     case 't':
       T=getT(NULL);
       if(T==0){
	 T=JDsyst();
	 break;
       }
       isFREEZ=1;
       break;
     case 'i':
       obsinfo(obs[actobs]);
       break;
     case 'd':
       derivtle(T, sats[actobs], 1);
       break;
     case '/':
       localsat=getsatname(sats[actobs]); 
       if(localsat!=NULL) {
	 if(Xen==1)
	   xmap(Xmap_SATDEL, 0, 0, 0, sats[actobs]->name);
	 sats[actobs]=localsat; 
       }
       break;
     case '0':
       horiz0[actobs]^=1;
       break;
     case 'm':
       multien^=1;
       break;
     case 'x':
       if(isX==1)
	 Xen^=1; 
       if(Xen==1) { 
	 xmap(Xmap_OBSADD, obs[0]->lon, obs[0]->lat, 0, obs[0]->name);
	 attron(COLOR_PAIR(5)); attron(A_BOLD); mvprintw(3, 10, "X"); }
       else {
	 mvprintw(3, 10, "  ");
	 xmap(Xmap_SATDEL, 0., 0., 0., sats[actobs]->name);
       }
       break;
     
     case 'f':
       isFT^=1;
       if(isFT==0) fastdir=1;
       break;
     case 'F':
       fastdir*=-1;
       break;
       
     case 'D':
       if(isDOP==1)
	 dopen^=0x01;
       
       if(dopen==1) { 
	 attron(COLOR_PAIR(5)); attron(A_BOLD); mvprintw(3, 14, "D"); }
       else mvprintw(3, 14, "  ");
       break;
       
       /* add an observer */
     case '+':
       if(numobs<5){
	 numobs++;
	 actobs=numobs; 
	 sats[actobs]=satlist;
	 obs[actobs]=getobs(0); 
	 derivtle(T, sats[actobs], 0);      /* if sat/obs are changed derived
					       tle window is no longer valid:
					       distroy it */ 
       }
       break;
       
       /* remove an observer */
       /* TODO: the selected observer should be removed, not the last... */
     case '-':
       if(numobs>0){
	 obs[numobs]=NULL;
	 sats[numobs]=NULL; 
	 numobs--;
	 actobs=numobs; 
	 derivtle(T, sats[actobs], 0);       /* if sat/obs are changed derived
						tle window is no longer valid:
						distroy it */ 
       }
       for(i=0; i<80; i++)
	 mvprintw(4+numobs,i, " ");
       break;
       
       /* on the fly introduction af a new observer */
     case 'o':
       if(numobs<5){
	 numobs++;
	 actobs=numobs;
	 derivtle(T, sats[actobs], 0);       /* of sat/obs are changed derived
						tle window is no longer valid:
						distroy it */ 
	 /* for the new observer a new element in the linked list 
	    has to be created reading obs.dat, as for command "+" */
	 sats[actobs]=satlist;
	 obs[actobs]=getobs(0); 
	 while(obs[actobs]->next!=NULL)
	   obs[actobs]=obs[actobs]->next;
	 /* the new observer can be specified both as lon, lat as qth locator,
	    recognition is automatic; heigth above sea level is let equal to
	    zero, but could be added */
	 attron(COLOR_PAIR(3)|A_BOLD);
	 curs_set(1);
	 mvprintw(3+numobs, 2, "QTH or lon,lat: "); 
	 timeout(-1);
	 echo();
	 mvgetnstr(3+numobs, 18, b, 78);
	 noecho();
	 timeout(0);
	 curs_set(0); 
	 
	 if((b[0]<='9' && b[0]>='0')||b[0]=='-'||b[0]=='+'){ 
	   sscanf(b, "%f %f", &imdlon, &imdlat);
	   if(lonlat2qthloc(imdlon, imdlat, imdqth)<0) {
	     for(i=0; i<80; i++) mvprintw(3+numobs,i, " ");
	     numobs--; actobs=numobs; break; }
	   else {
	     imdlon=imdlon/180.*M_PI;
	     imdlat=imdlat/180.*M_PI; 
	     imdqth[6]=0;
	     imdobs.lat=imdlat;
	     imdobs.lon=imdlon;
	     imdobs.h0=0.;
	     strcpy(imdobs.name, imdqth);
	     strcpy(imdobs.qth, imdqth);
	     strcpy(imdobs.descr, "On the fly supplied observer as lon,lat");
	   }
	 }
	 /* the name of the observer so introduced is the qthlocator itself */
	 else{
	   sscanf(b, "%s", imdqth);
	   if(qthloc2lonlat(imdqth, &imdlon, &imdlat)<0) { 
	     for(i=0; i<80; i++) mvprintw(3+numobs,i, " ");
	     numobs--; actobs=numobs;
	     break;
	   }
	   else {
	     imdlon=imdlon/180.*M_PI;
	     imdlat=imdlat/180.*M_PI; 
	     imdqth[6]=0;
	     imdobs.lat=imdlat;
	     imdobs.lon=imdlon;
	     imdobs.h0=0.;
	     strcpy(imdobs.name, imdqth);
	     strcpy(imdobs.qth, imdqth);
	     strcpy(imdobs.descr, "On the fly supplied observer as QTHloc");
	   }
	 }
       
	 /* and now add the structure to the linked list */
	 imdobs.next=NULL;
	 imdobs.prev=obs[actobs];
	 obs[actobs]->next=(OBS *)malloc(sizeof(OBS));
	 memcpy(obs[actobs]->next, &imdobs, sizeof(OBS));
	 obs[actobs]=obs[actobs]->next;
	 
       }
       break;
       
       
     case 27:
       if(getch()==91) 
	 switch(getch()){
	   
	 case 68:
	   if(sats[actobs]->prev!=NULL){ 
	     if(Xen==1)
	       xmap(Xmap_SATDEL, 0, 0, 0, sats[actobs]->name);
	     sats[actobs]=sats[actobs]->prev;
	   }
	   if(actobs==0) { 
	     roten=0; mvprintw(3, 12, "  ");
	   }
	   derivtle(T, sats[actobs], 0);   /* if sat/obs are changed derived
					      tle window is no longer valid:
					      distroy it */
	   break;                                         /* curs sx */
	   
	 case 67:
	   if(sats[actobs]->next!=NULL) {
	     if(Xen==1)
	       xmap(Xmap_SATDEL, 0, 0, 0, sats[actobs]->name);
	     sats[actobs]=sats[actobs]->next;
	   }
	   if(actobs==0) {
	     roten=0; mvprintw(3, 12, "  ");
	   }
	   derivtle(T, sats[actobs], 0);   /* if sat/obs are changed derived
					      tle window is no longer valid:
					      distroy it */
	   break;                                         /* curs dx */
	   
	 case 66:
	   if(actobs<numobs) {
	     actobs++; 
	     derivtle(T, sats[actobs], 0);
	   }                               /* if sat/obs are changed derived
					      tle window is no longer valid:
					      distroy it */
	   break;                                         /* curs up */
	   
	 case 65:
	   if(actobs>0){
	     actobs--; 
	     derivtle(T, sats[actobs], 0);
	   }                               /* if sat/obs are changed derived
					      tle window is no longer valid:
					      distroy it */ 
	   break;                                         /* curs down */
	   
	 case 53:
	   if(getch()!=126)
	     break;                                              /* pg up */
	   if(obs[actobs]->prev!=NULL && actobs!=0) 
	     obs[actobs]=obs[actobs]->prev;
	   break;
	   
	 case 54:
	   if(getch()!=126)
	     break;                                            /* pg down */
	   if(obs[actobs]->next!=NULL && actobs!=0) 
	     obs[actobs]=obs[actobs]->next;
	   break;
	   
	 case 49:
	 case 55:                      /* under rxvt/xterm Home change... */
	   if(getch()!=126)                                       /* Home */
	     break;
	   if(offset>0)
	     offset--;
	   break;
	   
	 case 52:
	 case 56:                       /* under rxvt/xterm End change... */
	   if(getch()!=126)                                        /* End */
	     break;
	   if(offset+30<=numsat)
	     offset++;
	   break;
	   
	 }           
       break;
       
     default:
       break;
     }
   }
   
   /* Visualization of used time (can be system time)*/
   DTstring(T, b, 1, 1, Zloc, 0, T);
   attroff(A_BOLD); 
   if(isFREEZ==1)
     attron(COLOR_PAIR(3)|A_REVERSE);
   else
     attron(COLOR_PAIR(3));
   mvprintw(0,50,"%s", b);
   if (isFREEZ==1)
     attroff(A_REVERSE);


/* sun and moon position calculation */
/* 
   it must be done here and not together visualization because these data
   are used in satellite illumination calulation
*/
 sunpos(T, obs[0], sunp);
 moonpos(T, obs[0], moonp);


/* multisatellite presentation*/
 multisat(T, sats[actobs], obs[actobs], ai, multien, offset);
 /* 
    if multisat presentation is on
    than normal presentation is active
    only for Home observer
 */
 if(multien==1) { 
   struct satdb *sdbp;
   sdbp=search_db(sats[0]); 
   attroff(A_BOLD); attron(COLOR_PAIR(3));
   mvprintw(0,0,"                       [%5d%c]",sats[0]->norad,
	    sats[0]->eclass);
   attron(COLOR_PAIR(6)|A_BOLD); 
   mvprintw(0,0,"%s", sats[0]->name);
   mvprintw(3, 0, "Home");
   single_sat_calc(T-J2000, sats[0], obs[0], asp);
   
  if (asp->squint<0)
    sprintf(sqs,"  --  ");
  else
    sprintf(sqs," %5.1f",asp->squint/M_PI*180);

  if(dopen==1)
    doppler(-(float)asp->rangerate, (float)asp->el);
  if(roten==1)
    rotor((float)asp->az, (float)asp->el);
  if(Xen==1)
    xmap(Xmap_SATSET,(float)asp->ssplon, (float)asp->ssplat, 
	 (float)asp->height, asp->name);
  attroff(A_BOLD);
  attron(COLOR_PAIR(1));
  mvprintw(2,20, 
	   "AZ     EL     Range      Hz / MHz  Squint   AOS/LOS   I  mEl");
  if(asp->el>localhoriz(obs[0], asp->az, horiz0[0])*M_PI/180.) 
    attron(COLOR_PAIR(2)|A_BOLD);
  if(sdbp->beacon[0]!=0.) 
    mvprintw(3,20,
	     "%5.1f  %5.1f %6.0f %+7.0f / %-5.0f%s             %c %4.1f",
	     asp->az/M_PI*180+180, asp->el/M_PI*180, 
	     asp->range, 
	     -asp->rangerate/3e5*sdbp->beacon[0]*1e6, sdbp->beacon[0], sqs,
	     sat_illum(*asp, *sunp),
	     localhoriz(obs[0], asp->az, horiz0[0]));   
  else
    mvprintw(3,20,
	     "%5.1f  %5.1f %6.0f     %6.3f     %s             %c %4.1f",
	     asp->az/M_PI*180+180, asp->el/M_PI*180, 
	     asp->range, asp->rangerate, sqs,
	     sat_illum(*asp, *sunp),
	     localhoriz(obs[0], asp->az, horiz0[0]));   
    
  if(asp->el<0.){
    attroff(A_BOLD); attron(COLOR_PAIR(1));
    attron(COLOR_PAIR(3));
    if(AOS(sats[0], obs[0], T-J2000, &tAOS)==0)
      DTstring(tAOS+J2000, b, 0, 1, Zloc, ai, T);
    else
      strcpy(b, " -------- ");
    mvprintw(3, 62, "%s", b);
  }
  else { 
    attroff(A_BOLD); attron(COLOR_PAIR(1));
    attron(A_BOLD); attron(COLOR_PAIR(3));
    if(LOS(sats[0], obs[0], T-J2000, &tLOS)==0)
      DTstring(tLOS+J2000, b, 0, 1, Zloc, ai, T);
    else 
      strcpy(b, " -------- ");
    mvprintw(3, 62, "%s", b);
  }
  
 }

 /* 
    if multisat presentation is off all observer and additional
    information are presented
 */
 else{
   /* Current satellite position calculation and visualization */
   for(i=0; i<=numobs; i++){
     struct satdb *sdbp;
     sdbp=search_db(sats[i]); 
     if(i==0){
       attroff(A_BOLD); attron(COLOR_PAIR(3));
       mvprintw(0,0,"                       [%5d%c]",sats[i]->norad,
		sats[i]->eclass);
       if(i==actobs)
	 attron(COLOR_PAIR(6)|A_BOLD); 
       else{
	 attroff(A_BOLD);
	 attron(COLOR_PAIR(6));
       }
       mvprintw(0,0,"%s", sats[i]->name);
       mvprintw(3, 0, "Home");
     }
     
     else{
       if(i==actobs)
	 attron(COLOR_PAIR(6)|A_BOLD); 
       else{
	 attroff(A_BOLD);
	 attron(COLOR_PAIR(6));
       }
       mvprintw(3+i, 0, "                    ");
       mvprintw(3+i, 0, "%s", obs[i]->name);
       mvprintw(3+i, strlen(obs[i]->name), " %s", sats[i]->name);
       attroff(A_BOLD);
       attron(COLOR_PAIR(3));
       mvprintw(3+i, strlen(obs[i]->name), "-");       
     }
     single_sat_calc(T-J2000, sats[i], obs[i], asp);
     
     if (asp->squint<0)
       sprintf(sqs,"  --  ");
     else
       sprintf(sqs," %5.1f",asp->squint/M_PI*180);

     
     if(i==0 && dopen==1)
       doppler((float)asp->rangerate, (float)asp->el);
     if(i==0 && roten==1)
       rotor((float)asp->az, (float)asp->el);
     if(Xen==1)
       xmap(Xmap_SATSET, (float)asp->ssplon, (float)asp->ssplat, 
	    (float)asp->height, asp->name);
     attroff(A_BOLD);
     attron(COLOR_PAIR(1));
     mvprintw(2,20, 
	      "AZ     EL     Range      Hz / MHz  Squint   AOS/LOS   I  mEl");
     if(asp->el>localhoriz(obs[i], asp->az, horiz0[i])*M_PI/180.) 
       attron(COLOR_PAIR(2)|A_BOLD);
     
     if(sdbp->beacon[0]!=0.) 
       mvprintw(3+i,20,
		"%5.1f  %5.1f %6.0f %+7.0f / %-5.0f%s             %c %4.1f",
		asp->az/M_PI*180+180, asp->el/M_PI*180, 
		asp->range, 
		-asp->rangerate/3e5*sdbp->beacon[0]*1e6, sdbp->beacon[0], sqs,
		sat_illum(*asp, *sunp),
		localhoriz(obs[0], asp->az, horiz0[0]));   
     else
       mvprintw(3+i,20,
		"%5.1f  %5.1f %6.0f     %6.3f     %s             %c %4.1f",
		asp->az/M_PI*180+180, asp->el/M_PI*180, 
		asp->range, asp->rangerate, sqs,
		sat_illum(*asp, *sunp),
		localhoriz(obs[0], asp->az, horiz0[0]));   
     
     if(i==actobs){
       attroff(A_BOLD);
       attron(COLOR_PAIR(3));
       mvprintw(10,0, 
	"Orbit   Phase  RA         Dec      Lat  <--SSP-->  Lon       Alt");
       mvprintw(11,0, "%6d  %5.1f  %s %8.3f  %7.3f    %8.3f %9.2f", 
		sats[i]->rev, asp->phase/M_PI*128., hms(asp->ra),
		asp->dec/M_PI*180., asp->ssplat/M_PI*180.,
		asp->ssplon/M_PI*180., asp->height);
     }
     
     /* AOS/LOS calculation and visualizzation */
     if(asp->el<0.){
       attroff(A_BOLD);
       attron(COLOR_PAIR(1));
       attron(COLOR_PAIR(3));
       if(AOS(sats[i], obs[i], T-J2000, &tAOS)==0)
	 DTstring(tAOS+J2000, b, 0, 1, Zloc, ai, T);
       else
	 strcpy(b, " -------- ");
       mvprintw(3+i, 62, "%s", b);
     }
     else { 
       attroff(A_BOLD);
       attron(COLOR_PAIR(1));
       attron(A_BOLD);
       attron(COLOR_PAIR(3));
       if(LOS(sats[i], obs[i], T-J2000, &tLOS)==0)
	 DTstring(tLOS+J2000, b, 0, 1, Zloc, ai, T);
       else
	 strcpy(b, " -------- ");
       mvprintw(3+i, 62, "%s", b);
     }
     
   }
 }

 /* Sun and Moon position visualization */
 attroff(A_BOLD);
 attron(COLOR_PAIR(1));
 if(sunp->el>0.)
   attron(COLOR_PAIR(2)|A_BOLD);
 mvprintw(21,0, "Sun:  RA %s  Dec %7.3f  Range %9.0f  AZ %5.1f  EL %5.1f",
	  hms(sunp->ra), sunp->dec/M_PI*180., sunp->range,
	  sunp->az/M_PI*180., sunp->el/M_PI*180.);
 
 attroff(A_BOLD);
 attron(COLOR_PAIR(1));
 if(moonp->el>0.)
   attron(COLOR_PAIR(2)|A_BOLD);
 mvprintw(22,0, "Moon: RA %s  Dec %7.3f  Range %9.0f  AZ %5.1f  EL %5.1f",
	  hms(moonp->ra), moonp->dec/M_PI*180., moonp->range,
	  moonp->az/M_PI*180., moonp->el/M_PI*180.);
 



/* wait 0.1 seconds before next calculation, but allow keypress to interrupt */
 struct timeval tv;
 fd_set readfds;
 FD_ZERO(&readfds);
   FD_SET(0,&readfds);
   tv.tv_sec=0;
   tv.tv_usec=100000;
   select(1,&readfds,NULL,NULL,&tv);
   
 }
 
 /* Exiting is better to clean everithing... */
 /* clear(); refresh(); */ /* If you like, I don't */
 free(asp);
 free(sunp);
 free(moonp);
 endwin(); 
 if(isROT==1) close(fdpoint);
 remove("el.dat");
 return 0;
}



/* database of frequency/mode has to be revised, when some stable policy will
be applied to current (amateur) satellite */
void read_db(FILE *fpdb,SAT *sats){
  char buff[80];
  int ok=0;
  int nr;
  satdb_empty.beacon[0]=0;
  satdb_empty.alon=-1000;
  
  satdbs=NULL;
  rewind(fpdb);
  while (fgets(buff, 78, fpdb)!=NULL) {
    /* If the line begin with '[' it's the satellite designation line */
    if (buff[0]=='[') {
      ok=0;
      nr = atoi(buff+1);
      if (nr>0) {
	/* found number; accept it */
	ok=1;
      } 
      else {
	/* found name; lookup NORAD number in *sats */
	SAT *s=sats; 
	int i;
	i=0;
	while(buff[i]!=']')
	  i++;
	while (s) {
	  if (strncasecmp(buff+1,s->name,i-1) == 0) {
	    ok=1;
	    nr=s->norad;
	    break;
	  }
	  s=s->next;
	}
      }
      
      if (ok) {
	struct satdb *s;
	s=(struct satdb *)malloc(sizeof(struct satdb));
	s->norad=nr;
	s->beacon[0]=0;
	s->alon=-1000;
	s->next=satdbs;
	satdbs=s;
      }
    } 
    /* if the line don't begin with '[' fields must be checked than get data*/
    else if (ok) {
      int l;
      l=0;
      while (isalnum(buff[l]))
	l++;
      if(l!=0) {
	if (strncasecmp(buff,"beacon", l)==0)
	  satdbs->beacon[0]=atof(buff+l);
	else if (0==strncasecmp(buff,"alon",l) || 
		 0==strncasecmp(buff,"blon",l)) 
	  satdbs->alon=atof(buff+l);
	else if (0==strncasecmp(buff,"alat",l) ||
		 0==strncasecmp(buff,"blat",l)) 
	  satdbs->alat=atof(buff+l);
	
      }       
    }
    
  }
  
}


struct satdb satdb_empty;

struct satdb *search_db(SAT *sats){
  struct satdb *s=satdbs;
  
  while (s) {
    if (s->norad==sats->norad) {
      return s;
    }
    s=s->next;
  }
  return &satdb_empty;
}


void rotor(float az, float el){  

  char buff[80]; 
  static double oldaz=0., oldel=0.; 
  int i;  
  
  if(el<0.)
    return;
  
  for(i=0; i<80; i++) buff[i]=0;
  if(fabs(az-oldaz)>0.5/180.*M_PI) {
    sprintf(buff, "AZ %.1f\n", az/M_PI*180.+180.);
    write(fdpoint, buff, strlen(buff));
    oldaz=az;
  }
 
  for(i=0; i<80; i++) buff[i]=0;
 if(fabs(el-oldel)>0.5/180.*M_PI) {
   sprintf(buff, "EL %.1f\n", el/M_PI*180.);
   write(fdpoint, buff, strlen(buff));
   oldel=el;
 }
 
}

void doppler(float rr, float el){
  
  char buff[80];
  
  if(el<0.){
    return;
  }
  
  sprintf(buff, "RR %f\n", rr*1e3); /* relative speed sat to obs [m/s] */
  write(fddop, buff, strlen(buff));
  
}


void passov(SAT *sats, OBS *obs, double t1, float t2){
#define MAXITER 100
  double tstart, tstop, howlong, tpi, tpe, azi, aze, 
    tmaxe, elm, tme1, tme2, tme3, tme4, e1, e2, e3, e4;
  char b[40], b1[40], b2[40], b3[40];
  satpos *sat;
  const float gs=0.61803399;
  int c;

  sat=(satpos*)malloc(sizeof(satpos));
  
  tstart=t1-J2000; howlong=t2/24.; 
  
  fprintf(stdout, "%s\t\t\tfor obs\t %s\n",sats->name, obs->name);
  fprintf(stdout, "Date              AOS @ AZ            tmaxEL @ EL\
        LOS@AZ\n");
  
  tstop=tstart+howlong;
  /* if at the end of given sched time el>0 than tstop is extended
     to setting */
  if(elev(tstop, sats, obs)>0.)  LOS(sats, obs, tstart+howlong, &tstop);
  /* but if LOS() cannot find a setting (and thus return zero), then tstop
     is used */
  if(tstop==0) tstop=tstart+howlong;
  
  if(elev(tstart, sats, obs)>0.) { 
   LOS(sats, obs, tstart, &tpe);
   tstart=tpe+115.7e-6;
  }

  tpe=tstart;
  while(tpe<tstop){
    AOS(sats, obs, tstart, &tpi); 
    if(tpi>tstop)
      break;
    DTstring(tpi+J2000, b1, 0, 1, Zloc, 0, 0);
    single_sat_calc(tpi, sats, obs, sat);
    azi=sat->az*180/M_PI+180.;
    LOS(sats, obs, tpi+115.7e-6, &tpe);
    DTstring(tpe+J2000, b2, 0, 1, Zloc, 0, 0);
    single_sat_calc(tpe, sats, obs, sat);
    aze=sat->az*180/M_PI+180.;
    DTstring(tpi+J2000, b, 1, 0, Zloc, 0, 0);

    /* searching for maximum elevation time and value */
   tme1=tpi+(tpe-tpi)/20.;
   tme3=tpe-(tpe-tpi)/20.;
   tme2=(tme1+tme3)/2.;

   c=0;
   do{
     c++;
     if(tme3-tme2>tme2-tme1)
       tme4=gs*(tme3-tme2)+tme2;
     else
       tme4=gs*(tme2-tme1)+tme1;
     e1=elev(tme1, sats, obs);
     e2=elev(tme2, sats, obs);
     e3=elev(tme3, sats, obs);
     e4=elev(tme4, sats, obs);
     min3of4(&tme1, &e1, &tme2, &e2, &tme3, &e3, tme4, e4);
   }while(fabs(tme1-tme3)>100e-6 && c<MAXITER);
   tmaxe=tme2;
   elm=e2;

   if(c<MAXITER){
     DTstring(tmaxe+J2000, b3, 0, 1, Zloc, 0, 0);
     
     fprintf(stdout, "%s%s @ %5.1f  %s @ %4.1f  %s @ %5.1f\n",
	     b, b1, azi, b3, elm, b2, aze);
   }
   else {
     fprintf(stdout, "%s%s @ %5.1f   --------- @ ----  %s @ %5.1f\n",
	     b, b1, azi,  b2, aze);
   }
   tstart=tpe+694.4e-6;
   /* iteration controll has been used to overcame the problem of double
      maximum in elevation: it can happen with molnjia orbit where Earth move
      faster than satellite in some occasion
      In this case maximum finding cicle can oscillate and never exit
      This has to be improved!
   */
 }

}


void sched(SAT *sats, OBS *obs, double t1, float t2, float t3){
  double t, start, tstart, tinc, tstop, tAOS, tLOS, howlong;
  FILE *fp, *fmt;
  char b[80], ill, sqs[10], lb[80], *tok;
  satpos sat, sun;
  int eop=0;
  
  /* This must go in file.c */
  if((fmt=fopen(cfgf.schedfmt, "r"))==0) { 
    printf("Unable to find schedule format file sched.fmt\n"); return; 
  }
  
  /* If t2==0 sched() has been called in interactive mode */
  if(t2==0){
    /* getT() is used to get absolute initial schedule time */
    attron(COLOR_PAIR(3));
    attroff(A_BOLD);
    curs_set(1);
    echo();
    timeout(-1);
    mvprintw(14,1,"Starting");
    start=getT(NULL)-J2000;
    /* getT() return zero if user supplied string for time is invalid */
    if(start==-J2000) { 
      mvprintw(14,1,"                                       ");
      return;
    }
    
    /* Other time are relative and are read so: */
    attron(COLOR_PAIR(3));
    attroff(A_BOLD);
    curs_set(1);
    echo();
    timeout(-1);
    mvprintw(14,1,"for [hours]: ");
    scanw("%lf",&howlong);
    howlong=howlong/24.;
    mvprintw(14,1,"                                       ");
    mvprintw(14,1,"deltaT [min]: ");
    scanw("%lf",&tinc);
    tinc/=1440.;
    mvprintw(14,1,"                                       ");
    mvprintw(14,1,"Calculating...");
    refresh();
    noecho();
    timeout(0);
    curs_set(0);
    fp=NULL;
  }
  
  /* sched() has been called in batch mode */
  else{
    start=t1-J2000;
    howlong=t2/24.;
    tinc=t3/1440.; 
    if((fp=fopen("/dev/stdout", "w"))==NULL) {
      printf("stdout - NULL \n");
      return;
    }
  }
  
  /* start with first rise or with given start time if already rised */
  if(elev(start, sats, obs)<0.)
    AOS(sats, obs, start, &tstart);
  else tstart=start;
  
  if(fp==NULL){
    if((fp=fopen("sched.dat","w"))==0){
      printf("unable to open sched.dat\n");
      return;
    }
  }
  fprintf(fp, "%s\t\t\tfor obs\t %s\n",sats->name, obs->name);
  
  tstop=tstart+howlong;
  /* if at the end of given sched time el>0 than tstop is extended to setting*/
  if(elev(tstop, sats, obs)>0.)
    LOS(sats, obs, tstart+howlong, &tstop);
  /* but if LOS() cannot find a setting (and thus return zero), then tstop
     is used */
  if(tstop==0)
    tstop=tstart+howlong;
  
  /* print the legend for output sched, according to sched.fmt */
  while(1) {
    if(fgets(lb, 79, fmt)==NULL)
      break;
    tok=lb;
    while(1){
      if(sscanf(tok, "%s", b)==EOF)
	break;
      if(strcasecmp(b, "date")==0)
	fprintf(fp, "Date               ");	
      else if(strcasecmp(b, "dateZ")==0)
	fprintf(fp, "DateZ              ");	
      else if(strcasecmp(b, "time")==0)
	fprintf(fp, "Time      ");
      else if(strcasecmp(b, "timeZ")==0)
	fprintf(fp, "TimeZ     ");
      else if(strcasecmp(b, "az")==0)
	fprintf(fp, "Az     ");
      else if(strcasecmp(b, "el")==0)
	fprintf(fp, "El     ");
      else if(strcasecmp(b, "ill")==0) 
	fprintf(fp, "Ill ");
      else if(strcasecmp(b, "sq")==0)
	fprintf(fp, "Sq    ");
      else if(strcasecmp(b, "lat")==0)
	fprintf(fp, "Lat   ");
      else if(strcasecmp(b, "lon")==0)
	fprintf(fp, "Lon   ");
      else if(strcasecmp(b, "range")==0)
	fprintf(fp, "Range ");
      else {
	fprintf(fp, "Unk ");
      }
      if((tok=index(tok, ' '))==NULL) break;
      tok++;
    }
    fprintf(fp, "\n");
  }
  rewind(fmt);
    

  
  for(t=tstart; t<=tstop; t+=tinc) {
    single_sat_calc(t, sats, obs, &sat);
    if(sat.squint<0)
      sprintf(sqs,"  --  ");
    else
      sprintf(sqs," %5.1f",sat.squint/M_PI*180);
    
    DTstring(t+J2000, b, 1, 1, Zloc, 0, 0);  
    
    /* needed for illumination calculation */
    sunpos(t+J2000, obs, &sun); 
    ill=sat_illum(sat, sun);
    
    if(sat.el<-0.05/180.*M_PI) {
      t-=tinc;
      LOS(sats, obs, t, &tLOS);
      if(tLOS!=0.) t=tLOS;
      DTstring(t+J2000, b, 1, 1, Zloc, 0, 0);
      single_sat_calc(t, sats, obs, &sat);
      eop=1;
    }
    
    
    /* schedule printing according to sched.fmt format */
    while(1) {
      if(fgets(lb, 79, fmt)==NULL)
	break;
      tok=lb;
      while(1) {
	if(sscanf(tok, "%s", b)==EOF)
	  break;
	if(strcasecmp(b, "date")==0) {
	  DTstring(t+J2000, b, 1, 0, 0, 0, 0);
	  fprintf(fp, "%s ", b);
	}
	else if(strcasecmp(b, "dateZ")==0) {
	  DTstring(t+J2000, b, 1, 0, 1, 0, 0);
	  fprintf(fp, "%s ", b);
	}
	else if(strcasecmp(b, "time")==0) {
	  DTstring(t+J2000, b, 0, 1, 0, 0, 0);
	  fprintf(fp, "%s ", b);
	}
	else if(strcasecmp(b, "timeZ")==0) {
	  DTstring(t+J2000, b, 0, 1, 1, 0, 0);
	  fprintf(fp, "%s ", b);
	}
	else if(strcasecmp(b, "az")==0) {
	  fprintf(fp, "%5.1f ", sat.az/M_PI*180+180);
	}
	else if(strcasecmp(b, "el")==0) {
	  fprintf(fp, "%5.1f ", sat.el/M_PI*180.);
	}
	else if(strcasecmp(b, "ill")==0) {
	  fprintf(fp, "%c ", ill);
	}
	else if(strcasecmp(b, "sq")==0) {
	  if (sat.squint<0) sprintf(sqs," --- ");
	  else sprintf(sqs," %5.1f",sat.squint/M_PI*180);
	  fprintf(fp, "%s ", sqs);
	}
	else if(strcasecmp(b, "range")==0) {
	  fprintf(fp, "%6.0f ", sat.range);
	}
	else if(strcasecmp(b, "lat")==0) {
	  fprintf(fp, "%5.1f ", sat.ssplat/M_PI*180.);
	}
	else if(strcasecmp(b, "lon")==0) {
	  fprintf(fp, "%5.1f ", sat.ssplon/M_PI*180.);
	}
	else {
	  fprintf(fp, "Unk ");
	}
	if((tok=index(tok, ' '))==NULL) break;
	tok++;
      }
      fprintf(fp, "\n");
    }
    rewind(fmt);
    if(eop==1) {
      t+=tinc; AOS(sats, obs, t, &tAOS); t=tAOS-tinc;
      fprintf(fp, "-----\n");
      eop=0;
    }    
    /*   fprintf(fp, "%s AZ= %5.1f  EL= %5.1f  Range= %6.0f Sq=%s Ill: %c\n",
	 b, sat.az/M_PI*180+180, sat.el/M_PI*180, sat.range, sqs, ill); */
  }
  
  if(t2==0)
    mvprintw(14,1,"                    ");
  fclose(fp); fclose(fmt);
}


void graphel(SAT *sats, OBS *obs, int isX){
  FILE *fp;
  double t, ti;
  char b[160], c[10];
  
  if((fp=fopen("el.dat","w"))==0) {
    printf("unable to open el.dat\n");
    return;
  }
  
  ti=JDsyst()-J2000;
  for(t=ti; t<ti+.9; t+=2.*M_PI/sats->n0/256.){
    UTCstring(t+J2000, b, Zloc); strncpy(c, b+3, 21); c[21]=0;
    fprintf(fp,"%s\t%4.1f\n", c, elev(t, sats, obs));
  }
  
  fclose(fp);

  if(isX==0)
    sprintf(b,"gnuplot %s >/dev/null 2>/dev/null", cfgf.gelf);
  else
    sprintf(b,"gnuplot %s >/dev/null 2>/dev/null", cfgf.gelfX);
  system(b);
}

/* if b==NULL stdin via ncurses is read, otherwise b is decoded */
double getT(char *bin){
  struct tm it;
  struct hr_time hr;
  char b[40], b1[40], b2[40];
  int n;
  
  if(bin==NULL) {
    attron(COLOR_PAIR(3)|A_BOLD);
    curs_set(1); 
    mvprintw(15, 1,
	     "[Date &] Time {dd/mm/yyyy hh:mm:[ss]} {hh:mm:[ss]}: "); 
    attroff(A_BOLD);
    timeout(-1);
    echo();
    mvgetnstr(15,53, b, 38);
   noecho();
   timeout(0);
   curs_set(0); 
   mvprintw(15, 1,
  "                                                                        "); 
  }

  else{
    strcpy(b, bin);
  }
  if(strlen(b)<1)
    return 0;
  if(strcmp(b, "now")==0){
    UTCstring(JDsyst(), b, 0);
    strcpy(b, b+5);
  }
  n=sscanf(b, "%s %s", b1, b2);
  it.tm_sec=0;  /* If seconds are not supplied this variable rest uninitialized
		   with unpredictable result... */
  it.tm_min=0;
  it.tm_hour=0; /* to be sure ... */
  switch(n){
  case 1:
    jd2hr(JDsyst(), &hr);
    strptime(b1, "%X", &it); 
    it.tm_year=hr.y;
    it.tm_mon=hr.mo-1;
    it.tm_mday=hr.dn;
    break;
    
  case 2:
    strcat (b1, " ");
    strcat(b1, b2); 
    strptime(b1, "%d/%m/%Y %T", &it);
    it.tm_year+=1900;
    break;

  case 0: 
  default:
    return 0;
  }

  hr.y=it.tm_year; hr.mo=it.tm_mon+1; hr.dn=it.tm_mday;
  hr.h=it.tm_hour; hr.mi=it.tm_min; hr.s=it.tm_sec; hr.ms=1;
  
  return (hr2jd(&hr));
}


/* offset is used to scroll the list, since on the screen there is place only
   for 30 satellite */
void multisat(double T, SAT *sats, OBS *obs, int ai, int endis, int offset){
  int i, j;
  satpos *msp, sun;
  char b[20], nb[30];
  double tALOS;
  static int olden=0;

  /* an ncurses window should be more elegant from programming point of
     view, but periodical update is visual annoying; doing so the visual
     effect is good */
  /* only if a transition beetween multi sat and single sat has occurred 
     than the shared portion of the screen has to be erased; doing it
     every time is visual anoying */
  if(olden!=endis) { 
   olden=endis;
   for(i=4; i<20; i++){
     move(i,0);
     deleteln();
     insertln();
   }
   refresh();
  }

  if(endis==0)
    return;

  msp=(satpos*)malloc(sizeof(satpos));    /*
					    structure containing the single
					    satellite for which calculus has 
					    been performed from time to time;
					    the problem of caluculus reduction
					    has been solved (thank to pa3fwm)
					    whit aos/los chaching routines 
					  */
  attron(A_BOLD);
  attron(COLOR_PAIR(5));
  mvprintw(4, 25, "Multi for obs: %-39s", obs->name); 

  /* To calculate illumination */
  sunpos(T, obs, &sun); 
  
  
  while(sats->prev!=NULL)
    sats=sats->prev;
  for(i=0; i<offset; i++)
    sats=sats->next;

  for(j=0; j<40; j+=39){
    for(i=0; i<15; i++) {
      if(sats==NULL)
	break;
      single_sat_calc(T-J2000, sats, obs, msp);
      attroff(A_BOLD); attron(COLOR_PAIR(1));
      if(msp->el>0.) attron(COLOR_PAIR(2)|A_BOLD);
     
      if(msp->el<0.){
	if(AOS(sats, obs, T-J2000, &tALOS)==0)
	  DTstring(tALOS+J2000, b, 0, 1, Zloc, ai, T);
	else 
	  strcpy(b, " -------- ");
      }
      else { 
	if(LOS(sats, obs, T-J2000, &tALOS)==0)
	  DTstring(tALOS+J2000, b, 0, 1, Zloc, ai, T);
	else
	  strcpy(b, " -------- ");
      }
      
      
     strcpy(nb, sats->name);
     nb[5]=0; /* Satellite name has to truncated */
     mvprintw(5+i, j, "%5s %5.1f %5.1f %6.0f %c %10s    ", 
	      nb, msp->az*180./M_PI+180., msp->el*180./M_PI, msp->range,
	      sat_illum(*msp, sun), b);
     sats=sats->next;
     
    }
  }
  
  free(msp);
}


/* 
   mode - 1: toggle show/delete
          0: delete the window
*/
void derivtle(double t, SAT *sats, int mode){
  static WINDOW *dtw;
  static int isshow=0;
  double a, tper;
  float x, y, z, rl;
  satpos *apos;
  OBS dummyobs;
  
 if(isshow==0&&mode==1){
   if((dtw=newwin(6, 79, 13, 0))==NULL)
     return;

   apos=(satpos*)malloc(sizeof(satpos));
   dummyobs.lon=0.;
   dummyobs.lat=0.;
   dummyobs.h0=0.; 

   single_sat_calc(t-J2000, sats, &dummyobs, apos);
   tper=(M_PI-apos->phase)/sats->n;
   tper=t-J2000+tper;
   single_sat_calc(tper, sats, &dummyobs, apos);

   wattron(dtw, COLOR_PAIR(3));
   wattroff(dtw, A_BOLD);
   box(dtw, ACS_VLINE, ACS_HLINE);
   mvwprintw(dtw, 0,10, "Derived orbital element value at system time for %s",
	     sats->name);
   wattron(dtw, COLOR_PAIR(6));
   wattroff(dtw, A_BOLD);
   mvwprintw(dtw, 1,72, "Using");
   mvwprintw(dtw, 2,73, "SGP4"); 
   mvwprintw(dtw, 3,73, "SDP4");
   mvwprintw(dtw, 4,72, "Model");

   wattron(dtw, COLOR_PAIR(3));
   wattroff(dtw, A_BOLD);
   mvwprintw(dtw, 1,2, "Period %8.2f min", 1440.*2.*M_PI/sats->n);
   mvwprintw(dtw, 1,26, "TLEage %5.1f day", t-J2000-sats->t0);
   mvwprintw(dtw, 1,46, "Sat age (~)    %5.0f day", sats->rev/sats->n*2.*M_PI);

   a=pow(sqrt(mu)/2./M_PI*86400.*2*M_PI/sats->n, 2./3.);
   /* calculate SSP, than from latitude get local earth radius and subtract
      it from perigee/apogee can make data maybe too accurate, using mean earth
      radius can give error up to 21.4Km */
   dummyobs.lon=apos->ssplon;
   dummyobs.lat=apos->ssplat;
   dummyobs.h0=0.;
   obsijk(tetha(t, apos->ssplon), &dummyobs, &x, &y, &z);
   rl=sqrt(x*x+y*y+z*z);
			 /* since apogee and perigee has SSP at latitude
			    changed in sign (longitude not), in the
			    reference geoid the same local radius hold */
   mvwprintw(dtw, 2,2, "S.M.A.  %8.1f Km", a);
   mvwprintw(dtw, 3,2, "Apogee  %8.1f Km", a*(1+sats->ecc)-rl);
   mvwprintw(dtw, 4,2, "Perigee %8.1f Km", a*(1-sats->ecc)-rl);
   
   mvwprintw(dtw, 2,26, "RAAN %7.3f deg", sats->node/M_PI*180.);
   mvwprintw(dtw, 3,26, "Incl %7.3f deg", sats->incl/M_PI*180.);
   mvwprintw(dtw, 4,26, "Ecc %8.5f", sats->ecc);
   
   mvwprintw(dtw, 2,46, "Arg Perigee %8.3f deg", sats->peri/M_PI*180.);
   
   mvwprintw(dtw, 3,46, "Lon Apogee  %8.3f deg", apos->ssplon/M_PI*180.);
   mvwprintw(dtw, 4,46, "Lat Apogee  %8.3f deg", apos->ssplat/M_PI*180.);
   
   wrefresh(dtw);
   isshow=1;
 }
 
 else if(isshow==1){
   wclear(dtw);  wrefresh(dtw); delwin(dtw); refresh();  isshow=0;
 }
 
}

SAT *getsatname(SAT *sats){
  char b[40];
  SAT *local;
  int i;
  
  attron(COLOR_PAIR(3));
  attroff(A_BOLD);
  curs_set(1); 
  mvprintw(15, 1, "Enter [part of] satellite name ");
  timeout(-1);
  echo();
  mvgetnstr(15, 32, b, 38);
  noecho();
  timeout(0);
  curs_set(0); 
  mvprintw(15, 1,
  "                                                                        "); 
  for(i=0; i<strlen(b); i++)
    b[i]=toupper(b[i]);

  local=sats;
  while(local->prev!=NULL)
    local=local->prev;
  while(local!=NULL) {
    /*   if(strncmp(b, local->name, sizeof(b))==0) return local; */
    /* doing so the initial part of the name will be searched, but seems more
       usefull to search the substring */
    if(strstr(local->name, b)!=NULL)
      return local;
    local=local->next;
  } 
  return NULL;
}


/* a general method for not to overwritte window should be adopted, or if this
   will happen shold be put isshow=0 from the overwritter */
void obsinfo(OBS *obsi){
  static WINDOW *oiw;
  static int isshow=0;

  if(isshow==0){
    if((oiw=newwin(6, 79, 13, 0))==NULL)
      return;
    attron(COLOR_PAIR(3));
    attroff(A_BOLD);
    box(oiw, ACS_VLINE, ACS_HLINE);
    mvwprintw(oiw, 1,1, "[%10s]    Lat %8.3f    Lon %8.3f    Height %4.0f     \
QTH: %s", obsi->name, obsi->lat*180./M_PI, obsi->lon*180./M_PI
	      , obsi->h0, obsi->qth);
    mvwprintw(oiw, 2,2, "\"%s\"", obsi->descr);
    wmove(oiw, 3,2);
    wrefresh(oiw);
    isshow=1;
  }

 else{
   wclear(oiw);
   wrefresh(oiw);
   delwin(oiw);
   refresh();
   isshow=0;
 }
}




/* calling this function must not block calculation, since while reading help
   rotor controll should not be halted; 'h' could be used to toggle hsw() but 
   a check on overwrite should be done */
void help(void){  
  WINDOW *hsw;
 
  timeout(-1);
  if((hsw=newwin(8, 79, 13, 0))==NULL)
    return;
  box(hsw, ACS_VLINE, ACS_HLINE);

  mvwprintw(hsw, 0,37, " Keystroke ");
  mvwprintw(hsw, 1,2, "g - Graph EL display");
  mvwprintw(hsw, 2,2, "h - Help (this screen)");
  mvwprintw(hsw, 3,2, "r - Rotor (toggle)");
  mvwprintw(hsw, 4,2, "a - Abs/Rel time(toggle)");
  mvwprintw(hsw, 5,2, "Curs U/D - Set active obs");
  mvwprintw(hsw, 6,2, "f - Fast");
  
  mvwprintw(hsw, 1,29, "q - Quit TRK");
  mvwprintw(hsw, 2,29, "s - Schedule");
  mvwprintw(hsw, 3,29, "z - UTC/Local (toggle)");
  mvwprintw(hsw, 4,29, "o - Set new observer");
  mvwprintw(hsw, 5,29, "Pg U/D - Prev/Next obs");
  mvwprintw(hsw, 6,29, "F - FF/Rev in fast mode");
  
  mvwprintw(hsw, 1,53, "Space - Suspend");
  mvwprintw(hsw, 2,53, "Curs L/R - Prev/Next sat");
  mvwprintw(hsw, 3,53, "+/- - Add/Remove Obs");
  mvwprintw(hsw, 3,53, "t - Calc at fixed time");
  mvwprintw(hsw, 4,53, "m - Multisat (toggle)");
  mvwprintw(hsw, 5,53, "0 - Real horizon (toggle)");
  mvwprintw(hsw, 6,53, "Home/End - Scroll multisat");
  
  wrefresh(hsw);
  
  getch();
  timeout(0);
  wclear(hsw);
  wrefresh(hsw);
  delwin(hsw);
  refresh();
}

void xmap(int funct, float lon, float lat, float h, char *name){
  char buff[80]; 
  static float oldlon=0., oldlat=0., oldh=0.; 
  const float minvar=0.5/180*M_PI, minvarh=5.;

  switch(funct){

  case Xmap_SATSET:
    if(fabs(lon-oldlon)<minvar && fabs(lat-oldlat)<minvar &&
       fabs(h-oldh)<minvarh){
      return;
    }
    
    sprintf(buff, "s %f\t%f\t%f\t%s\n",lon*180./M_PI, lat*180./M_PI, h, name);
    write(fdxmap, buff, strlen(buff));
    oldlon=lon;
    oldlat=lat; oldh=h;
    break;
    
  case Xmap_SATDEL:
   sprintf(buff, "s- %s\n", name);
   write(fdxmap, buff, strlen(buff));
   break;
   
  case Xmap_OBSADD:
    sprintf(buff, "o %f\t%f\t%s\n",lon*180./M_PI, lat*180./M_PI, name);
    write(fdxmap, buff, strlen(buff));
    break;
    
  case Xmap_OBSDEL:
    sprintf(buff, "o- %s\n", name);
    write(fdxmap, buff, strlen(buff));
    break;
    
  /* default: */
  }
}

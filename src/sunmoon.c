/*
 * TRK - Satellite tracking program based on Norad SGP/SDP model with
 *       curses interface
 *
 *	by Lapo Pieri IK5NAX  2000-2020
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *  Send bugs reports, comments, critique, etc, to ik5nax@radioteknos.it
 */

#include <math.h>
#include "main.h"


struct pertermoon{
  int D;
  int M;
  int M1;
  int F;
  double k;
};

const struct pertermoon ptml[60]={
  {0,	0,	1,	0,	6288774.},
  {2,	0,	-1,	0,	1274027.},
  {2,	0,	0,	0,	658314.	},
  {0,	0,	2,	0,	213618.	},
  {0,	1,	0,	0,	-185116.},
  {0,	0,	0,	2,	-114332.},
  {2,	0,	-2,	0,	58793.	},
  {2,	-1,	-1,	0,	57066.	},
  {2,	0,	1,	0,	53322.	},
  {2,	-1,	0,	0,	45758.	},
  {0,	1,	-1,	0,	-40923.	},
  {1,	0,	0,	0,	-34720.	},
  {0,	1,	1,	0,	-30383.	},
  {2,	0,	0,	-2,	15327.	},
  {0,	0,	1,	2,	-12528.	},
  {0,	0,	1,	-2,	10980.	},
  {4,	0,	-1,	0,	10675.	},
  {0,	0,	3,	0,	10034.	},
  {4,	0,	-2,	0,	8548.	},
  {2,	1,	-1,	0,	-7888.	},
  {2,	1,	0,	0,	-6766.	},
  {1,	0,	-1,	0,	-5163.	},
  {1,	1,	0,	0,	4987.	},
  {2,	-1,	1,	0,	4036.	},
  {2,	0,	2,	0,	3994.	},
  {4,	0,	0,	0,	3861.	},
  {2,	0,	-3,	0,	3665.	},
  {0,	1,	-2,	0,	-2689.	},
  {2,	0,	-1,	2,	-2602.	},
  {2,	-1,	-2,	0,	2390.	},
  {1,	0,	1,	0,	-2348.	},
  {2,	-2,	0,	0,	2236.	},
  {0,	1,	2,	0,	-2120.	},
  {0,	2,	0,	0,	-2069.	},
  {2,	-2,	-1,	0,	2048.	},
  {2,	0,	1,	-2,	-1773.	},
  {2,	0,	0,	2,	-1595.	},
  {4,	-1,	-1,	0,	1215.	},
  {0,	0,	2,	2,	-1110.	},
  {3,	0,	-1,	0,	-892.	},
  {2,	1,	1,	0,	-810.	},
  {4,	-1,	-2,	0,	759.	},
  {0,	2,	-1,	0,	-713.	},
  {2,	2,	-1,	0,	-700.	},
  {2,	1,	-2,	0,	691.	},
  {2,	-1,	0,	-2,	596.	},
  {4,	0,	1,	0,	549.	},
  {0,	0,	4,	0,	537.	},
  {4,	-1,	0,	0,	520.	},
  {1,	0,	-2,	0,	-487.	},
  {2,	1,	0,	-2,	-399.	},
  {0,	0,	2,	-2,	-381.	},
  {1,	1,	1,	0,	351.	},
  {3,	0,	-2,	0,	-340.	},
  {4,	0,	-3,	0,	330.	},
  {2,	-1,	2,	0,	327.	},
  {0,	2,	1,	0,	-323.	},
  {1,	1,	-1,	0,	299.	},
  {2,	0,	3,	0,	294.	},
  {2,	0,	-1,	-2,	0.	}
};

const struct pertermoon ptmr[60]={
  {0,	0,	1,	0,      -20905355.},
  {2,	0,	-1,	0,	-3699111.},
  {2,	0,	0,	0,	-2955968.},
  {0,	0,	2,	0,	-569925.},
  {0,	1,	0,	0,	48888.},
  {0,	0,	0,	2,	-3149.},
  {2,	0,	-2,	0,	246158.},
  {2,	-1,	-1,	0,	-152138.},
  {2,	0,	1,	0,	-170733.},
  {2,	-1,	0,	0,	-204586.},
  {0,	1,	-1,	0,	-129620.},
  {1,	0,	0,	0,	108743.},
  {0,	1,	1,	0,	104755.},
  {2,	0,	0,	-2,	10321.},
  {0,	0,	1,	2,	0.},
  {0,	0,	1,	-2,	79661.},
  {4,	0,	-1,	0,	-34782.},
  {0,	0,	3,	0,	-23210.},
  {4,	0,	-2,	0,	-21636.},
  {2,	1,	-1,	0,	24208.},
  {2,	1,	0,	0,	30824.},
  {1,	0,	-1,	0,	-8379.},
  {1,	1,	0,	0,	-16675.},
  {2,	-1,	1,	0,	-12831.},
  {2,	0,	2,	0,	-10445.},
  {4,	0,	0,	0,	-11650.},
  {2,	0,	-3,	0,	14403.},
  {0,	1,	-2,	0,	-7003.},
  {2,	0,	-1,	2,	0.},
  {2,	-1,	-2,	0,	10056.},
  {1,	0,	1,	0,	6322.},
  {2,	-2,	0,	0,	-9884.},
  {0,	1,	2,	0,	5751.},
  {0,	2,	0,	0,	0.},
  {2,	-2,	-1,	0,	-4950.},
  {2,	0,	1,	-2,	4130.},
  {2,	0,	0,	2,	0.},
  {4,	-1,	-1,	0,	-3958.},
  {0,	0,	2,	2,	0.},
  {3,	0,	-1,	0,	3258.},
  {2,	1,	1,	0,	2616.},
  {4,	-1,	-2,	0,	-1897.},
  {0,	2,	-1,	0,	-2117.},
  {2,	2,	-1,	0,	2354.},
  {2,	1,	-2,	0,	0.},
  {2,	-1,	0,	-2,	0.},
  {4,	0,	1,	0,	-1423.},
  {0,	0,	4,	0,	-1117.},
  {4,	-1,	0,	0,	-1571.},
  {1,	0,	-2,	0,	-1739.},
  {2,	1,	0,	-2,	0.},
  {0,	0,	2,	-2,	-4421.},
  {1,	1,	1,	0,	0.},
  {3,	0,	-2,	0,	0.},
  {4,	0,	-3,	0,	0.},
  {2,	-1,	2,	0,	0.},
  {0,	2,	1,	0,	1165.},
  {1,	1,	-1,	0,	0.},
  {2,	0,	3,	0,	0.},
  {2,	0,	-1,	-2,	8752.}
};


const struct pertermoon ptmb[60]={
  {0,	0,	0,	1,	5128122.},
  {0,	0,	1,	1,	280602.},
  {0,	0,	1,	-1,	277693.},
  {2,	0,	0,	-1,	173237.},
  {2,	0,	-1,	1,	55413.},
  {2,	0,	-1,	-1,	46271.},
  {2,	0,	0,	1,	32573.},
  {0,	0,	2,	1,	17198.},
  {2,	0,	1,	-1,	9266.},
  {0,	0,	2,	-1,	8822.},
  {2,	-1,	0,	-1,	8216.},
  {2,	0,	-2,	-1,	4324.},
  {2,	0,	1,	1,	4200.},
  {2,	1,	0,	-1,	-3359.},
  {2,	-1,	-1,	1,	2463.},
  {2,	-1,	0,	1,	2211.},
  {2,	-1,	-1,	-1,	2065.},
  {0,	1,	-1,	-1,	-1870.},
  {4,	0,	-1,	-1,	1828.},
  {0,	1,	0,	1,	-1794.},
  {0,	0,	0,	3,	-1749.},
  {0,	1,	-1,	1,	-1565.},
  {1,	0,	0,	1,	-1491.},
  {0,	1,	1,	1,	-1475.},
  {0,	1,	1,	-1,	-1410.},
  {0,	1,	0,	-1,	-1344.},
  {1,	0,	0,	-1,	-1335.},
  {0,	0,	3,	1,	1107.},
  {4,	0,	0,	-1,	1021.},
  {4,	0,	-1,	1,	833.},
  {0,	0,	1,	-3,	777.},
  {4,	0,	-2,	1,	671.},
  {2,	0,	0,	-3,	607.},
  {2,	0,	2,	-1,	596.},
  {2,	-1,	1,	-1,	491.},
  {2,	0,	-2,	1,	-451.},
  {0,	0,	3,	-1,	439.},
  {2,	0,	2,	1,	422.},
  {2,	0,	-3,	-1,	421.},
  {2,	1,	-1,	1,	-366.},
  {2,	1,	0,	1,	-351.},
  {4,	0,	0,	1,	331.},
  {2,	-1,	1,	1,	315.},
  {2,	-2,	0,	-1,	302.},
  {0,	0,	1,	3,	-283.},
  {2,	1,	1,	-1,	-229.},
  {1,	1,	0,	-1,	223.},
  {1,	1,	0,	1,	223.},
  {0,	1,	-2,	-1,	-220.},
  {2,	1,	-1,	-1,	-220.},
  {1,	0,	1,	1,	-185.},
  {2,	-1,	-2,	-1,	181.},
  {0,	1,	2,	1,	-177.},
  {4,	0,	-2,	-1,	176.},
  {4,	-1,	-1,	-1,	166.},
  {1,	0,	1,	-1,	-164.},
  {4,	0,	1,	-1,	132.},
  {1,	0,	-1,	-1,	-119.},
  {4,	-1,	0,	-1,	115.},
  {2,	-2,	0,	1,	107.}
};


void moonpos(double JD, OBS *obs, satpos *moonp){

#define NTERM 60
  
  double t, L1, D, M, M1, F, A1, A2, A3, sl, sr, sb, lambda, beta, eps, Rm;
  double x, y, z, ra, dec, az, el;
  float xo, yo, zo;
  int i;
  
  t=(JD-2451545.0)/36525.; 
  L1=3.810341023+8399.709113*t-2.3157e-5*t*t+
    3.23904e-8*t*t*t-1.56771e-10*t*t*t*t;
  D=5.198466741+7771.377147*t-2.8449e-5*t*t+
    3.19735e-8*t*t*t-1.56804e-11*t*t*t*t;
  M=6.240060127+628.3019552*t-2.6808e-6*t*t+7.1267e-10;
  M1=2.355555899+8328.691426*t+1.5703e-4*t*t+
    2.5041e-7*t*t*t-1.1863e-9*t*t*t*t;
  F=1.627905233+8433.466158*t-5.9392e-5*t*t-
    4.9499e-9*t*t*t+2.02167e-11*t*t*t*t;
  
  A1=2.09003+2.3012*t;
  A2=0.9266+8364.73985*t;
  A3=5.4707+8399.68473*t;
  
  /* Reduction of earth orbit eccentricity is ignored, for now */
  sl=0.;
  sr=0.;
  sb=0.; 
  for(i=0; i<NTERM; i++){
    sl+=ptml[i].k*sin(ptml[i].D*D+ptml[i].M*M+ptml[i].M1*M1+ptml[i].F*F);
    sr+=ptmr[i].k*cos(ptmr[i].D*D+ptmr[i].M*M+ptmr[i].M1*M1+ptmr[i].F*F);
    sb+=ptmb[i].k*sin(ptmb[i].D*D+ptmb[i].M*M+ptmb[i].M1*M1+ptmb[i].F*F);
  }
  
  
  sl+=3958.*sin(A1)+1962*sin(L1-F)+318*sin(A2);
  sb+=-2235*sin(L1)+382*sin(A3)+175*sin(A1-F)+175*sin(A1+F)+
    127*sin(L1-M1)-115*sin(L1+M1);
  
  
  /* Transformation fromm lambda, beta to ra,dec has to be put in coord.c */
  lambda=L1+sl/1e6/180.*M_PI;
  beta=sb/1e6/180.*M_PI;
  eps=0.409092804-2.269655e-4*t-2.86e-9*t*t-8.7897e-9*t*t*t; 
  
  ra=atan2(sin(lambda)*cos(eps)-tan(beta)*sin(eps), cos(lambda));
  dec=asin(sin(beta)*cos(eps)+cos(beta)*sin(eps)*sin(lambda));
  
  Rm=385000.56+sr/1e3;
  
  radec2ijk(ra, dec, &x, &y, &z);
  moonp->x=Rm*x; moonp->y=Rm*y; moonp->z=Rm*z; 
  
  radec2azel(ra, dec, tetha(JD, obs->lon/M_PI*180.), obs->lat, &az, &el);
  
  moonp->ra=ra; moonp->dec=dec; moonp->az=az; moonp->el=el;
  
  obsijk(tetha(JD, obs->lon/M_PI*180.), obs, &xo, &yo, &zo);
  
  moonp->range=sqrt((moonp->x-xo)*(moonp->x-xo)+(moonp->y-yo)*(moonp->y-yo)+
		    (moonp->z-zo)*(moonp->z-zo));
  
}

void sunpos(double JD, OBS *obs, satpos *sunp){
  
  double t, L0, M, e, C, O, l, ep, ec, Rs;
  double x, y, z, ra, dec, az, el;
  float xo, yo, zo;
  
  t=(JD-2451545.0)/36525.; 
  L0=4.895063+628.3319668*t+5.2918e-6*t*t;
  M=6.24006+628.301955*t-2.720968e-6*t*t-8.378e-9*t*t*t;
  e=0.016708617-42.037e-6*t-123.6e-9*t*t; 
  C=(0.033416074-84.07251e-6*t-244.35e-9)*sin(M)+
    (348.94e-6-1.7628e-6*t)*sin(2.*M)+5.06145e-6*sin(3.*M);
  O=2.18236-33.75704*t;
  l=L0+C-99.31e-6-83.43e-6*sin(O);
  ep=0.4090928-226.97e-6*t-2.86e-9*t*t+8.79e-9*t*t*t; 
  ec=ep+44.68e-6*cos(O);
  

  dec=asin(sin(ec)*sin(l));
  ra=atan2(cos(ec)*sin(l), cos(l));
  
  Rs=149.60015e6*(1-e*e)/(1+e*cos(M+C));
  
  radec2ijk(ra, dec, &x, &y, &z);
  sunp->x=Rs*x;
  sunp->y=Rs*y;
  sunp->z=Rs*z; 
 
  radec2azel(ra, dec, tetha(JD, obs->lon/M_PI*180.), obs->lat, &az, &el);
 
  sunp->ra=ra;
  sunp->dec=dec;
  sunp->az=az;
  sunp->el=el;
 
  obsijk(tetha(JD, obs->lon/M_PI*180.), obs, &xo, &yo, &zo);
  
  sunp->range=sqrt((sunp->x-xo)*(sunp->x-xo)+(sunp->y-yo)*(sunp->y-yo)+
		   (sunp->z-zo)*(sunp->z-zo));
}

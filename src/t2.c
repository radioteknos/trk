/*
 * TRK - Satellite tracking program based on Norad SGP/SDP model with
 *       curses interface
 *
 *	by Lapo Pieri IK5NAX  2000-2020
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *  Send bugs reports, comments, critique, etc, to ik5nax@radioteknos.it
 */

/* Many things to revise:
   3) join time.c and t2.c
   4) use isleap() declared in /usr/include/bsd/tzfile.h instead of leap()
      rewritten here
   5) convert all the routine in sgp.c so to use JD instead of JD-J200
*/

#include <stdio.h>
#include <time.h>
#include <sys/time.h>
#include <sys/timeb.h>
#include <math.h>
#include <string.h>
#include "main.h"

const double JD1970=2440587.5;
const char dow[7][4]={"Sun","Mon","Tue","Wed","Thu","Fri","Sat"};


struct gc_time TZsyst;

/* TODO: create t2.h */
int leap(int);


/*       JDsyst:                                                          
	 IN: ---                                                    
	 OUT: actual julian day deriveed from system clock */
double JDsyst(void){

  double jd;
  struct timespec st;
  
  clock_gettime(CLOCK_REALTIME, &st);
  
  jd=JD1970+st.tv_sec/86400.+st.tv_nsec/86400e9;
  
  TZsyst.tzme=localtime(&st.tv_sec)->tm_gmtoff/60;
  
  return jd;
}


/*       leap:                                                              
	 IN:  year                                                   
	 OUT: 1 if leap, 0 otherwise                           
	 
	 !!! ONLY FOR GREGORIAN CALENDAR !!! */
int leap(int y){
  return ((y%4==0)&&(y%100!=0))||((y%4==0)&&(y%100==0)&&(y%400==0));
}	  



/*       hr2jd:                                                            
	 IN: structure with date and time in readable form
	 OUT: julian day (JD)                                      */

double hr2jd(struct hr_time *hr){

  int y, m, a, b;
  double jd;
  
  y=hr->y;
  m=hr->mo;
  
  /* to distinguish between julian and gregorian calendar */
  if(y<1582) b=0;
  else if(y<=1582){
    if(m<10)
      b=0; 
    else if(m<=10){
      /* TODO: understand */
      /* ?! */
      if(hr->dn<4)
	b=0;
      else if(hr->dn<=4)
	b=0; 
      /* ?! */
    } 
  }
  else{
    a=(int)(y/100);
    b=2-a+(int)(a/4);
  }
  
  /* start of day year shifted to March 1 */
  if(m<=2){
    y--;
    m+=12;
  }
  
  /* julian day calculation */
  jd=(double)((int)(365.25*(y+4716))+
	      (int)(30.6001*(float)(m+1))+hr->dn+b-1524.5+
	      (double)hr->h/24.+(double)hr->mi/1440.+(double)hr->s/86400.+
	      (double)hr->ms/86.4e6);
 
  return jd;
}



/*       jd2hr:                                                           
	 IN: julian day (JD)                                      
	 OUT: structure with date and time in readable format  */
void jd2hr(double jd, struct hr_time *hr){

  double z, F;
  int a, A, B, C, D, E, Z;
  
  F=modf(jd+0.5,&z);
  Z=(int)z;
  if(Z<2299161)
    A=Z;
  else{
    a=(int)((Z-1867216.25)/36524.25);
    A=Z+1+a-(int)(a/4);
  }
  B=A+1524; C=(int)((B-122.1)/365.25); 
  D=(int)(365.25*C); E=(int)((B-D)/30.6001);
  
  hr->dn=B-D-(int)(30.6001*E);
  if(E<14)
    hr->mo=E-1;
  else
    hr->mo=E-13;
  if(hr->mo>2)
    hr->y=C-4716;
  else
    hr->y=C-4715;
  
  hr->h=(int)(F*24); 
  hr->mi=(int)((F-hr->h/24.)*1440.); 
  hr->s=(int)((F-hr->h/24.-hr->mi/1440.)*86400.);
  hr->ms=(int)((F-hr->h/24.-hr->mi/1440.-hr->s/86400.)*86400000.);
  
  hr->dw=(int)fmod(jd+1.5,7.);
  hr->dy=(int)(275.*hr->mo/9.)-(2-leap(hr->y))*(int)((hr->mo+9)/12.)+hr->dn-30;
}




/*       UTCstring:                                                         
	 IN: julian day (JD), isZ=1 for UTC, 0 for local time
	 OUT: string that describe date and time in UTC/local
	 
	 (is this ISO8661 compliant ?)   */
/* Use of DT() is preferred */
void UTCstring(double jd, char *utcs, int isZ){

  struct hr_time hr;
  
  if(isZ==0) jd+=(double)TZsyst.tzme/1440.;
  jd2hr(jd, &hr);
  
  /*
    sprintf(utcs, "%s  %.2d/%.2d/%.2d   %.2d:%.2d:%.2d.%.3d",
    dow[hr.dw], hr.dn, hr.mo, hr.y, hr.h, hr.mi, hr.s, hr.ms);
    if(isZ==1) strstr(utcs, "Z");
  */
  /* Millisecond are seldom usefull in this use... maybe add a parameter to
     add ms or not */
  
  sprintf(utcs, "%s  %.2d/%.2d/%.2d   %.2d:%.2d:%.2d",
	  dow[hr.dw], hr.dn, hr.mo, hr.y, hr.h, hr.mi, hr.s);
  if(isZ==1)
    strcat(utcs, "Z");
  if(isZ==0)
    strcat(utcs, " ");
}


/*       DATE/TIMEstring:                                                 
	 jd     - jiulian day (JD)                    
	 *dts   - string with date and/or time          
	 isDATE - 1 if date required            
	 isTIME - 1 if time required                     
	 isZ    - 1 for time in UTC              
	 isABS  - 1 for absolute time, otherwise all the other
	 is* must be zero
	 T      - reference time if isABS=0   
*/

/* to do things better reference time should be in respect to a given instant,
   so isABS should be modified in isREL and make so that with 0 absolute time
   is used, with <0 the reference is JDsyst(), and with >0 the reference is
   that value */
void DTstring(double jd, char *dts, int isDATE, int isTIME, int isZ, 
	      int isABS, double T){

  struct hr_time hr;
  char b[80];
  
  *dts='\0';
  
  if(isABS) { 
    jd-=T; hr.h=(int)(jd*24.); hr.mi=(int)(jd*1440.-hr.h*60.);
    hr.s=(int)(jd*86400-hr.h*3600.-hr.mi*60.);
    sprintf(dts, "+%.2d:%.2d:%.2d ", hr.h, hr.mi, hr.s);
    return;
  }
  
  else if(isZ==0)
    jd+=(double)TZsyst.tzme/1440.;
  
  jd2hr(jd, &hr);
  
  if(isDATE){
    sprintf(b, "%s  %.2d/%.2d/%.4d  ", 
	    dow[hr.dw], hr.dn, hr.mo, hr.y); strcat(dts, b); }
  
  if(isTIME){
    sprintf(b, " %.2d:%.2d:%.2d", hr.h, hr.mi, hr.s); 
    strcat(dts, b);
    if(isZ&&!isABS)
      strcat(dts, "Z"); 
    else
      strcat(dts, " "); }
  
}




/*       tetha:                                     
	 IN:  julian day (JD), longitude [deg]
	 OUT: sideral time [rad] (0-2Pi)    */
double tetha(double jd, float lon){

  double t, teg, th;

  t=(jd-2451545.)/36525.;
  /* this polinomial should be evaluated in a more efficient way */
  teg=280.46061837+360.98564736629*(jd-2451545)+0.000387933*t*t-t*t*t/38710000;
  th=(teg+lon)/180.*M_PI;
  
  /* wrap arround 0-2*Pi */
  th-=(int)(th/2/M_PI)*2.*M_PI;
  
  return(th); 
}                        



/*       hms:
	 IN:  angle [rad]
	 OUT: pointer to a string containing hour, minutes and second */
char *hms(double a){
  
  static char b[30];
  int h, m, s;
  
  a=a*12./M_PI;
  while(a>24.)
    a-=24;
  while(a<0)
    a+=24.;
  h=(int)a;
  m=(int)((int)(a*60)-h*60);
  s=(int)((int)(a*3600)-m*60-h*3600);
  
  sprintf(b, "%2dh%02dm%02ds", h, m, s);
  return b;
}

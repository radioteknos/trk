/*
 * TRK - Satellite tracking program based on Norad SGP/SDP model with
 *       curses interface
 *
 *	by Lapo Pieri IK5NAX  2000-2020
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *  Send bugs reports, comments, critique, etc, to ik5nax@radioteknos.it
 */

#include <string.h>
#include <ctype.h>
#include "main.h"

/* many good article has been written on QTH locator, in italian a good one
   is on RadioRivista 1/91 pag 77 */

/* brief notes on QTH locator:
   - the larger rectangle is 20 by 10 degrees (lon by lat) [first two letters]
   - the intermediate is 2 by 1 degree [figure]
   - the smaller 5 by 2.5 arcminutes [last two letters]
   - the first character in every couple is longitude
   - for the first couple of letters are valid AA-RR, for the second AA-XX
   - prior to this locator there was an other...
   - largher square numeration start with A on south pole and end with R on
     north pole
   - longitude starts with A at 180 degrees from Greenwich and end with R on
     meridian zero 
                                                                             */

/*  lonlat2qthloc:      (longitude,latitude)->QTH Locator
                   IN: lon [deg]  {-180,360} <0 west   >0 east               
                       lat [deg]  {-90,90} <0 south   >0 north
                   OUT: qthlocator [capital letter]
                   Return value: -1 if lon,lat are out of range, 0 otherwise */
int lonlat2qthloc(float lon, float lat, char *qth){

  float t;
  
  if(lon>360 || lon<-180 || lat>90 || lat<-90)
    return(-1);
  
  if(lon>180.)
    lon-=360.;

  lon+=180.;
  lat+=90.;
  t=lon/20.;

  qth[0]='A'+(int)t;
  t=(t-(int)t)*10;
  qth[2]='0'+(int)t;
  t=(t-(int)t)*24;
  qth[4]='A'+(int)t;
  t=lat/10.;
  qth[1]='A'+(int)t;
  t=(t-(int)t)*10;
  qth[3]='0'+(int)t;
  t=(t-(int)t)*24;
  qth[5]='A'+(int)t;
  
  return(0);
}



/*  Qthloc2lonlat:      QTH Locator -> (longitude,latitude)         
                  IN: qthlocator [upper or lower case]                   
		  OUT: lon [deg]  {-180,180} <0 west   >0 east             
		       lat [deg]  {-90,90}   <0 south  >0 north                
		  Return value: -1 if qth has wrong character, 0 otherwise
                                                                         
ATTENTION: returned coordinates are the center of the square, not the a border!

If QTH is given of only 4 character last two will be set to KK (center of
larger sqare) */
int qthloc2lonlat(char *qth, float *lon, float *lat){

  qth[0]=toupper(qth[0]);
  qth[1]=toupper(qth[1]);
 
  if(qth[0]<'A' || qth[0]>'R')
    return(-1);
  if(qth[1]<'A' || qth[1]>'R')
    return(-1);
  if(qth[2]<'0' || qth[2]>'9')
    return(-1);
  if(qth[3]<'0' || qth[3]>'9')
    return(-1);

  if(strlen(qth)<6){
    qth[4]='k';
    qth[5]='k';
  }

  qth[4]=toupper(qth[4]);
  qth[5]=toupper(qth[5]);

  if(qth[4]<'A' || qth[4]>'X')
    return(-1);
  if(qth[5]<'A' || qth[5]>'X')
    return(-1);

  *lon=(qth[0]-'A')*20.+(qth[2]-'0')*2.+(qth[4]-'A')/12.+1./24.;
  *lat=(qth[1]-'A')*10.+(qth[3]-'0')*1.+(qth[5]-'A')/24.+1./48.;
  *lon-=180; *lat-=90;   

  return 0;
}

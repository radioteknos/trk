/*
 * TRK - Satellite tracking program based on Norad SGP/SDP model with
 *       curses interface
 *
 *	by Lapo Pieri IK5NAX  2000-2020
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *  Send bugs reports, comments, critique, etc, to ik5nax@radioteknos.it
 */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>
#include "main.h"

extern cfg_file cfgf;

/* calcuation of the local horizon (due to obstacle, hill, building...) as
   specified in the elevation table inside obs.dat file */
double localhoriz(OBS *obs, double az, int h0){
  int n=0, i;
  
  if(h0==0)
    return 0.;
  
  az=az*180./M_PI+180;
  
  /* search for the to row in the table just greater and just less then az */
  while(obs->horiz[0][n]!=360.){
    if(obs->horiz[0][n]<az)
      i=n;
    if(obs->horiz[0][n]>az)
      break;
    n++;
    if(n>72)
      break;
  }

  /* linear interpolation between the closest point of az */
  return((obs->horiz[1][n]-obs->horiz[1][i])/
	 (obs->horiz[0][n]-obs->horiz[0][i])*
	 (az-obs->horiz[0][i])+obs->horiz[1][i]);
}


/* observer position calculation in equatorial geocentric system (IJK) 
   with ellipsoidal model for Earth globe */
/* Which WGS has been used? Check! */
void obsijk(double tetha, OBS *obs, float *xo, float *yo, float *zo){
 
  float x,z;
  
  x=(aT/sqrt(1-eT*eT*sin(obs->lat)*sin(obs->lat))+obs->h0)*cos(obs->lat);
  z=(aT/sqrt(1-eT*eT*sin(obs->lat)*sin(obs->lat))*(1-eT*eT)+obs->h0)*
    sin(obs->lat);
  *xo=x*cos(tetha);
  *yo=x*sin(tetha);
  *zo=z;
}


/* get information about observer from obs.dat and fill the relative 
   structures that are organized as a linked list, performing error checking.
   
   If prerr=1 errors are printed on stderr, otherwise go ahead: can happen that
   the linked list has no data (this could be signaled, or maybe a list with
   only a NULL pointer result (?))
*/
OBS *getobs(int prerr){
  
  FILE *fpobs;
  char lb[80], b[80];
  int n, e=1, chk, i;
  OBS *obs, *h, *t;
  float v, lon, lat, horaz, horel;
  
  if((fpobs=fopen(cfgf.obsdatf, "r"))==NULL) {
    if(prerr==1) 
      fprintf(stderr, "Unable to open observer data file %s\n", cfgf.obsdatf); 
    return NULL;
  }
  
  obs=NULL;
  h=NULL;
  t=NULL;
  
  while(e){
    for(n=0; n<80; n++)
      lb[n]=0;
    
    /* check for EOF */
    if(fgets(lb, 78, fpobs)==NULL) { e=0; lb[0]='['; }
    
    /* character # is for comments */
    if(lb[0]=='#') continue;
    
    /* if character "[" has been found ... */
    if(lb[0]=='[') {          
      if(obs!=NULL) {              /* but it isn't the first observer */
                                   /* 
				      than the obs structure can be saved 
				      in the linked list:it's not completely
				      clear to me but it's similar to sgp.c
				      from COLA and it works...
				   */
	
	/* first of all do some check and fill unspecified field */
	chk=0;
	
	/* if lon and lat hasn't been read, than try to obtain from QTH */
	if(obs->lat>90.&&obs->lon>180.){
	  if(obs->qth[0]==0) chk=1;                   /* no lon, lat, neither
							 qthloc */
	  else if(qthloc2lonlat(obs->qth, &lon, &lat)<0) chk=2; /* Error on
								   format of
								   QTHloc */
	  else{
	    obs->lon=lon;
	    obs->lat=lat;
	  }
	}

	/* otherwise check if lon,lat agree with QTH */
	else if(obs->qth[0]!=0) {
	  lonlat2qthloc(obs->lon, obs->lat, b);
	  if(strcmp(b, obs->qth)!=0)
	    chk=3;
	}

	/* if QTH isn't in data must be calculated */
	else{
	  lonlat2qthloc(obs->lon, obs->lat, b);
	  strcpy(obs->qth, b);
	}


	/* some other check could be done, e.g. if only lon or only lat
	   are present, but this seems to be enough */
	
	/* if data are correct, save in the linked list */
	obs->lat=obs->lat/180.*M_PI; obs->lon=obs->lon/180.*M_PI;
	
	if(chk==0){
	  if(h==NULL)
	    h=obs;
	  if(t!=NULL)
	    t->next=obs;
	  obs->prev=t;
	  t=obs;
	}
	
	/* if, otherwise, data aren't correct no need to do anything, except
	   for error signaling, if request */
	else{
	  switch(chk){
	  case 1:
	    if(prerr==1) 
	      fprintf(stderr, "No lon, lat, neither QTHloc for [%s]!\n",
		      obs->name); break;
	  case 2:  
	    if(prerr==1) 
	      fprintf(stderr, "Error in QTHloc format for [%s]\n", obs->name);
	    break;
	  case 3:
	    if(prerr==1) {
	      qthloc2lonlat(obs->qth, &lon, &lat);
	      fprintf(stderr, "QTHloc do not match for [%s]: should be %s or \
lon %7.3f, lat %7.3f\n", obs->name, b, lon, lat);
	    }
	    break;
	    
	  default:
	    if(prerr==1) fprintf(stderr, "Unknown error!\n");
	    break;
	  }
	
	}
      }

      /* now memory can be reserved for a new structure; with linnked list
	 method is not necessary to free memory with free(obs) */
      obs=(OBS *)malloc(sizeof(OBS));
      
      /* unvalid or null data are imposed to check what has been assigned
	 before to save the structure */
      obs->lat=91.;
      obs->lon=181.;
      obs->h0=0.;      
      obs->qth[0]=0;
      obs->descr[0]=0;
      obs->name[0]=0;
      obs->horiz[0][0]=0.; obs->horiz[1][0]=0.;
      obs->horiz[0][1]=360.; obs->horiz[1][1]=0.;
      n=1;
      if(e!=0){
	while(lb[n]!=']'){
	obs->name[n-1]=lb[n];
	n++;
      }
      obs->name[n-1]=0;
      }
    }

    
    /* if otherwise the row don't begin with a "#", neither a "[", than
       data must be analyzed */
    else{      
      
      /* String should be trasformed to upper */
      if(strncmp(lb, "lon", strlen("lon"))==0){ 
  	if(sscanf(lb+strlen("lon")+1, "%f", &v)!=EOF)
	  obs->lon=v;	
	else 
	  obs->lon=181.;
      }
      if(strncmp(lb, "lat", strlen("lat"))==0){ 
  	if(sscanf(lb+strlen("lat")+1, "%f", &v)!=EOF)
	  obs->lat=v;	
	else
	  obs->lat=91.;
      }
      if(strncmp(lb, "height", strlen("height"))==0){ 
  	if(sscanf(lb+strlen("height")+1, "%f", &v)!=EOF)
	  obs->h0=v;
	else
	  obs->h0=0.;
      }
      if(strncmp(lb, "QTHloc", strlen("QTHloc"))==0){ 
  	if(sscanf(lb+strlen("QTHloc")+1, "%s", b)!=EOF)
	  strcpy(obs->qth, b);
	else
	  obs->qth[0]=0;
      }
      /* TODO: I don't like this string elaboration, there are no check
	 on length, is assumed that a single character (0x0d) will terminate
	 the row; could be done better, even if it work */
      if(strncmp(lb, "Descr", strlen("Descr"))==0){ 
  	strncpy(obs->descr, lb+strlen("Descr")+1,
		strlen(lb+strlen("Descr")+1)-1);
	obs->descr[strlen(lb+strlen("Descr")+1)-1]=0;
      }
      if(strncmp(lb, "Horiz", strlen("Horiz"))==0){ 
	i=0;
	while(1){
	  if(fgets(lb, 78, fpobs)==NULL){
	    e=0;
	    lb[0]='[';
	    break;
	  }
	  
	  sscanf(lb, "%f %f", &horaz, &horel);
	  obs->horiz[0][i]=horaz; obs->horiz[1][i]=horel; i++;
	  
	  if(i>70)
	    break;
	  if(strncmp(lb, "Horiz_end", strlen("Horiz_end"))==0)
	    break;
	}
	obs->horiz[0][i]=360.; obs->horiz[1][i]=obs->horiz[1][0];
      }
      
      /* if the field is not recognized, simply ignore it
	 TODO: could be done better */
    }
  }
  
  fclose(fpobs);
  
  /* the begin of linked list is h */
  return h;
} 

/*
 * TRK - Satellite tracking program based on Norad SGP/SDP model with
 *       curses interface
 *
 *	by Lapo Pieri IK5NAX  2000-2020
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *  Send bugs reports, comments, critique, etc, to ik5nax@radioteknos.it
 */

#include <math.h>
#include "main.h"

/* using the navigation definition of azimuth: 0 for NORTH */

/* coordinates transformation from geocentric (IJK) to local topocentric 
   (SEZ) system; angles are in radian, theta is the local sidereal time */
void ijk2sez(double i, double j, double k, double lat, double theta,
	     double *s, double *e, double *z){

  *s=sin(lat)*cos(theta)*i+sin(lat)*sin(theta)*j-cos(lat)*k;
  *e=-sin(theta)*i+cos(theta)*j;
  *z=cos(lat)*cos(theta)*i+cos(lat)*sin(theta)*j+sin(lat)*k;
  
}


/* TODO */
void sez2ijk(double s, double e, double z, double lat, double theta,
	     double *i, double *j, double *k){
  
}

/* coordinates transformation from local topocentric (SEZ) to local
   altazimuthal (AZ, EL) system; angles are in radians */
void sez2azel(double s, double e, double z,
	      double *az, double *el, double *r){
  
  *r=sqrt(s*s+e*e+z*z);
  *el=asin(z/ *r);
  *az=atan2(-e, s);
}

/* coordinates transformation from celestial equatorial (RA, Dec) to
   altazimuthal (AZ, EL) system; angles are in radians, theta is the local
   sideral time */
void radec2azel(double ra, double dec, double theta, double lat,
		double *az, double *el){
  double h;
  
  h=theta-ra;                                           /* hour angle */
  *az=atan2(sin(h), cos(h)*sin(lat)-tan(dec)*cos(lat))+M_PI;
  *el=asin(sin(lat)*sin(dec)+cos(lat)*cos(dec)*cos(h));
}

/* coordinates transformation from local altazimuthal (AZ, EL) to
   celestial equatorial (RA, Dec) system; angles are in radians, theta 
   is the local sideral time */
void elaz2radec(double az, double el, double theta, double lat,
		double *ra, double *dec){

  *ra=theta-atan2(-sin(az), tan(el)*cos(lat)-cos(az)*sin(lat));
  *dec=asin(sin(lat)*sin(el)+cos(lat)*cos(el)*cos(az));
}

/* coordinates transformation from local topocentric (IJK) to
   celestial equatorial (RA, Dec) system; angles are in radians */
void ijk2radec(double i, double j, double k, double *ra, double *dec){

  *ra=atan2(j, i);
  *dec=asin(k/sqrt(i*i+j*j+k*k));

}

/* coordinates transformation from celestial equatorial (RA, Dec) to
   local topocentric (IJK) system; angles are in radians, topocentric system 
   distance from Earth's center is assumed to be unity */
void radec2ijk(double ra, double dec, double *in, double *jn, double *kn){

  *in=cos(ra);
  *jn=sin(ra);
  *kn=sin(dec);

}

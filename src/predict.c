/*
 * TRK - Satellite tracking program based on Norad SGP/SDP model with
 *       curses interface
 *
 *	by Lapo Pieri IK5NAX  2000-2020
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *  Send bugs reports, comments, critique, etc, to ik5nax@radioteknos.it
 */

#include <math.h>
#include <stdlib.h>
#include "main.h"

typedef struct _cache {
  SAT *sat;
  OBS *obs;
  double tinit;
  double tALOS;
  struct _cache *next;
} Cache;

#define MAX_CACHE_LEN 150

static Cache *aos_cache=NULL;
static Cache *los_cache=NULL;

static int check_cache(SAT *, OBS *, double, double *, Cache **);
static void add_cache(SAT *, OBS *, double, double, Cache **);



/*
  Parameter:
  *sats - data structur of satellite (from TLE)
  *obs  - observer data structure
  tinit - time for calculation start in JD-J2000
  *tAOS - time for next rise in JD-J2000
  
  Returned values:
  0 - no error
  1 - satellite already rised
  2 - satellite will not rise in the next 72h
*/
int AOS(SAT *sats, OBS *obs, double tinit, double *tAOS){
  
  double t1, t2, t3, e1, e2, e3, d12, d23, ext1, ext2, t, tstart;
  int n;
  const int NMAX=100;
  
  /* if t<0 calculation starts from system time for calculating next rise */
  if(tinit<0)
    tinit=JDsyst()-J2000;
  
  if(check_cache(sats,obs,tinit,tAOS,&aos_cache))
    return 0;
  
  if(elev(tinit, sats, obs)>0.){
    *tAOS=0;
    return 1;
  }

  /* start with evaluating elevation in 3 consecutive instants following 
     actual time, has be seen that 1/20 of orbital period works */
  t1=tinit; e1=elev(t1, sats, obs);
  t2=t1+2.*M_PI/sats->n0/20.; e2=elev(t2, sats, obs);
  t3=t2+2.*M_PI/sats->n0/20.; e3=elev(t3, sats, obs);
  

  tstart=t1;

  /* such 3 instant (t1, t2, t3) are advanced up to the incremental ratio 
     beetween two adiacent couple change sign: that is an extermum; percision
     is low, doing so, but enough to find other exterme and the bisect among 
     them to find AOS */
  n=0;
  while((d12=(e2-e1)/(t2-t1))*(d23=(e3-e2)/(t3-t2))>0.){
    t1=t2; t2=t3;
    t3+=2.*M_PI/sats->n0/20.;
    e1=elev(t1, sats, obs);
    e2=elev(t2, sats, obs);
    e3=elev(t3, sats, obs); 
    if((t3-tstart)>3.||n++>NMAX){
      *tAOS=0.;
      return 2;
    }
 }
  
  /* if the found extremum has a negative elevation search the next extremum
     and let the first extremum (ext1) equal to the last extremum found */
  if(elev(t2, sats, obs)<0.){
    do{
      ext1=t2;
    
      t1=ext1; e1=elev(t1, sats, obs);
      t2=t1+2.*M_PI/sats->n0/20.; e2=elev(t2, sats, obs);
      t3=t2+2.*M_PI/sats->n0/20.; e3=elev(t3, sats, obs);
      
      n=0;
      while((d12=(e2-e1)/(t2-t1))*(d23=(e3-e2)/(t3-t2))>0.){
	t1=t2;
	t2=t3;
	t3+=2.*M_PI/sats->n0/20.;
	e1=elev(t1, sats, obs);
	e2=elev(t2, sats, obs);
	e3=elev(t3, sats, obs); 
	
	if((t3-tstart)>3.||n++>NMAX){
	  *tAOS=0.;
	  return 2;
	}
      }
      
      ext2=t2;
    }while(elev(ext1, sats, obs)*elev(ext2, sats, obs)>0.);
    /* this search continue until two extreme, one with negative elevation and
       the following with positive elevation, are found */
  }

  /* if the first extremum has positive elevation as first "extremum" will
     be used the actual time */
  else{
    ext1=tinit;
    ext2=t2;
  }
  

 /* identical procedure used for LOS, zero crossing of elevation is searched
    with bisection algorithm, once known the extreme */
  t1=ext1;
  t2=ext2;
  while((t2-t1)*86400>.5){
    t=(t1+t2)/2.;
    if(elev(t1, sats, obs)*elev(t, sats, obs)<0.)
      t2=t;
   else
     t1=t;
 }
   t=(t1+t2)/2.;
   /* The result is returned as time JD-J2000 in variable tAOS */
   *tAOS=t;

   add_cache(sats,obs,tinit,*tAOS,&aos_cache);
   return 0;
}


/*
Parameter:
  *sats - satellite data structure (from TLE)
  *obs  - observer data structure
  tinit - initial time from wich search LOS (in JD-J2000)
  *tLOS - time of the next set (LOS) (in JD-J2000)

Returned Value:
  0 - no problem
  1 - satellite already set
  2 - satellite don't set in the next 72 hour
*/
int LOS(SAT *sats, OBS *obs, double tinit, double *tLOS){
  double t1, t2, t3, t, e1, e2, e3, tstart, tend, ir;

  /* if tint<0 use the system time as time at wich begin calculation */
  if(tinit<0)
    tinit=JDsyst()-J2000;

  if(check_cache(sats,obs,tinit,tLOS,&los_cache))
    return 0;

  t1=tinit;
  if(elev(t1, sats, obs)<0.){
    *tLOS=0;
    return 1;
  }

  tstart=t1;
  t2=t1+.5*(2.*M_PI/sats->n0); 
  t=(t1+t2)/2.;

  /* for low orbit the above guess for the time when elev()<0 work, but for
     molnjia or quasi-geo orbits the final extremum for bisection must be
     found more accurately */
  if(elev(t1, sats, obs)*elev(t2, sats, obs)>0.){
   /* first set the time to stop the search, since the search algorithm go
      ahead up to find a minimum (seem that four orbital period can go) */
   tend=t1+(2.*M_PI/sats->n0)*4.; 
   e1=elev(t1, sats, obs); 
   t2=t1+2.*M_PI/sats->n0/20.;
   e2=elev(t2, sats, obs);
   t3=t2+2.*M_PI/sats->n0/20.;
   e3=elev(t3, sats, obs);
   
   /* minimum search go for succesive step up to a sign reversal in susequent
      incremetal ratio is found; to avoid to fall in a maximum, instead, 
      molnjia type orbits do this and more...) is checked also that if
      incremental ratio change sign elevation must be less than zero. And go
      ahead up to this condition don't match... */
   while((ir=(e2-e1)/(t2-t1)*(e3-e2)/(t3-t2))>0.||
	 (ir<0.&&elev(t2,sats, obs)>0.)){
     t1=t2;
     t2=t3;
     t3+=2.*M_PI/sats->n0/20.;
     e1=elev(t1, sats, obs);
     e2=elev(t2, sats, obs);
     e3=elev(t3, sats, obs); 
     /* ... except if time is too much in the futeur, as happen in 
	geostationary sats over the horizon. Maybe a more expressive than
	??:??:?? could be signaled. */
     if(t3>tend){
       *tLOS=0;
       return 2;
     }
   }
   /* note: for bisection the second extremum is t2, while for the last
      search is t3 */
   t2=t3;
  } 

  /* if an accurate search for the second extremum has be done, than t1 is
     altered, in every case it's better to restore it. */
  t1=tstart; 

  /* this is the bisection; the only problem could be in some very particular
     case, maybe in molnjia orbits, where a minimum is very close to zero 
     crossing of elevation. */
  while((t2-t1)*86400>.5){
    t=(t1+t2)/2.;
    if(elev(t1, sats, obs)*elev(t, sats, obs)<0.)
      t2=t;
    else
      t1=t;
  }
  t=(t1+t2)/2.;
   
  /* result is returned as JD-J2000 time in variable tLOS */
  *tLOS=t;
 
  add_cache(sats,obs,tinit,*tLOS,&los_cache);
  return 0;
}



/* returns 0 if not found in cache, 1 otherwise */
static int check_cache(SAT *sat, OBS *obs, double tinit, double *tALOS,
		       Cache **cachep){
  Cache **c;

  c=cachep;
  while(*c){
    /* walk through the list */
    if((*c)->sat==sat          /* right satellite? */
       && (*c)->obs==obs          /* right observer? */
       && tinit>=(*c)->tinit      /* right time-interval? */
       && tinit<=(*c)->tALOS){
      Cache *tmp;
      /* we've found an appropriate record; move to head of list */
      tmp=*c;
      *c=tmp->next;
      tmp->next=*cachep;
      *cachep=tmp;
      /* and return result */
      *tALOS=tmp->tALOS;
      return 1;
    }
    c=&(*c)->next;
  }
  return 0;
}


static void add_cache(SAT *sat, OBS *obs, double tinit, double tALOS,
		      Cache **cachep){
  Cache *c;
  int l;

  /* create new record */
  c=malloc(sizeof(Cache));
  if(c==NULL)
    return;
  c->sat=sat;
  c->obs=obs;
  c->tinit=tinit;
  c->tALOS=tALOS;
  /* attach to head of list */
  c->next=*cachep;
  *cachep=c;

  /* count length of list */
  l=1;
  while((*cachep)->next){
    l++;
    cachep=&(*cachep)->next;
  }
  if(l>MAX_CACHE_LEN){
     /* remove tail: the record that has not been used for the longest time */
    free(*cachep);
    *cachep = NULL;
  }
}

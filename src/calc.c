/*
 * TRK - Satellite tracking program based on Norad SGP/SDP model with
 *       curses interface
 *
 *	by Lapo Pieri IK5NAX  2000-2020
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *  Send bugs reports, comments, critique, etc, to ik5nax@radioteknos.it
 */

#include <math.h>
#include <string.h>
#include "main.h"

#define thetas 4.652418e-3  /* Sun's semidiameter as seen from Earth */


/* It seems working, though has been adjusted with "cut and try" style... 
   Theory should be revised and formulas should be rewritten for clarity  */ 
char sat_illum(satpos sat, satpos sun){
  double rhoe, rhos, rs, thetae, theta, costheta;

  rhoe=sqrt(sat.x*sat.x+sat.y*sat.y+sat.z*sat.z);
  rs=sqrt(sun.x*sun.x+sun.y*sun.y+sun.z*sun.z);
  rhos=sqrt((sat.x-sun.x)*(sat.x-sun.x)+(sat.y-sun.y)*(sat.y-sun.y)+
	    (sat.z-sun.z)*(sat.z-sun.z));

  thetae=asin(R/rhoe);
  costheta=(sat.x*(sat.x-sun.x)+sat.y*(sat.y-sun.y)+sat.z*(sat.z-sun.z))/
    rhoe/rhos;
  theta=acos(costheta);
  

  if(theta<thetae-thetas && costheta>0.)
    return 'U'; /* Umbral Eclipse */
  
  if(fabs(thetae-thetas)<theta && theta<thetae+thetas && costheta>0.)
    return 'P'; /* Penumbral Eclipse */
  
  /* The satellite is visible since it is over the horizon and the sun 
     has -6 deg of elevation */
  if(sat.el>0. && sun.el<-0.1047) 
    return 'V'; /* Visual Visible */
  
  /* The satellite is not visible sinec the sun is too high */
  if(sat.el>0. && sun.el>=-0.1047) 
    return 'S'; /* in Sunlight */
  
  /* The satellite is illuminated but not in sigth */
  return 'I'; /* Illuminated */
  
  
  /* It should not to be difficult to find passeges aganist the sun (and moon) 
     disk, checking tha when el>0 az,el sat (better to do it with ra,dec?) 
     are no more than thetas from az,el from sun (or moon) */
}


/* Calculation of elevation only for a given sat and observer, maybe this 
   attempt to economize calculation is not so important */
double elev(double t, SAT *sats, OBS *obs){
  double theta, rx, ry, rz, rS, rE, rZ, r;
  float xo, yo, zo;
  
  theta=tetha(t+J2000, obs->lon/M_PI*180.);
  (void) (*sgp4)(sats, (double)t);
  obsijk(theta, obs, &xo, &yo, &zo);
  
  rx=sats->x[0]*R-xo; ry=sats->x[1]*R-yo; rz=sats->x[2]*R-zo; 
  rS=sin(obs->lat)*cos(theta)*rx+sin(obs->lat)*sin(theta)*ry-cos(obs->lat)*rz;
  rE=-sin(theta)*rx+cos(theta)*ry;
  rZ=cos(obs->lat)*cos(theta)*rx+cos(obs->lat)*sin(theta)*ry+sin(obs->lat)*rz;
  
  r=sqrt(rS*rS+rE*rE+rZ*rZ);
  
  return(180./M_PI*asin(rZ/r));
}


/* Main calculation for a single satellite and observer, that call SGP code */
void single_sat_calc(double t, SAT *sats, OBS *obs, satpos *asp){
  double thetav, rx, ry, rz, rS, rE, rZ, r, vx, vy, vz, az, el;
  float xo, yo, zo;
  const float oe=72.9212352e-6;
  double ra, dec, ssplat, ssplon, h;
  struct satdb *satdbp;
  
  thetav=tetha(t+J2000, obs->lon/M_PI*180.);
  (void) (*sgp4)(sats, (double)t);
  obsijk(thetav, obs, &xo, &yo, &zo);

  rx=sats->x[0]*R-xo;
  ry=sats->x[1]*R-yo;
  rz=sats->x[2]*R-zo; 

  asp->x=sats->x[0]*R;
  asp->y=sats->x[1]*R;
  asp->z=sats->x[2]*R;

  ijk2sez(rx, ry, rz, obs->lat, thetav, &rS, &rE, &rZ);

  sez2azel(rS, rE, rZ, &az, &el, &r);

  asp->az=az;
  asp->el=el;
  asp->range=r;

  vx=sats->dx[0]*R/86400.;
  vy=sats->dx[1]*R/86400.;
  vz=sats->dx[2]*R/86400.;
  
  asp->rangerate=((vx+oe*yo)*rx+(vy-oe*xo)*ry+vz*rz)/r;
  asp->norad=sats->norad;
  asp->class=sats->eclass;
  strcpy(asp->name, sats->name);
  
  asp->phase=sats->mean-sats->peri+2*M_PI;
  if(asp->phase<0)
    asp->phase+=2*M_PI;
  asp->phase=fmod(asp->phase, 2.*M_PI);
  
  ijk2radec(rx, ry, rz, &ra, &dec);
  sub_sat_point(sats->x[0]*R, sats->x[1]*R, sats->x[2]*R,
		t+J2000, &ssplat, &ssplon, &h);
  asp->ra=ra; asp->dec=dec; 
  asp->ssplon=ssplon;
  asp->ssplat=ssplat;
  asp->height=h;
  
  satdbp = search_db(sats);
  if (satdbp->alon==-1000)
    asp->squint=-1;
  else {
    double ax,ay,az;
    double bx,by,bz;
    double cx,cy,cz;
    double alon,alat;
    
    /* calculate antenna angle with line-of-nodes instead of with major
       axis, and convert to radians */
    alon=satdbp->alon*M_PI/180 + sats->peri;    
    alat=satdbp->alat*M_PI/180;
    /* calculate unit vector in orbit plane coordinates: X to asc.node, 
       Z perp.to orbit plane */
    bx=cos(alat)*cos(alon);
    by=cos(alat)*sin(alon);
    bz=sin(alat);
    /* transform to coordinate system with X still to asc.node, 
       Z along earth's rotation axis */
    cx=bx;
    cy=by*cos(sats->incl) - bz*sin(sats->incl);
    cz=by*sin(sats->incl) + bz*cos(sats->incl);
    /* transform to coordinate system with X to "First Point in 
       Aries", i.e., the standard celestial coordinates ijk */
    ax=cx*cos(sats->node) - cy*sin(sats->node);
    ay=cx*sin(sats->node) + cy*cos(sats->node);
    az=cz;
    /* cos(squint) is now just minus the inner product of (ax,ay,az)
       and (rx,ry,rz) normalised */
    asp->squint=acos ( - (ax*rx+ay*ry+az*rz) / sqrt(rx*rx+ry*ry+rz*rz) );
  }
  
#ifdef DEBUG
  mvprintw(16,1,"xs= %8.1f", sats->x[0]*R);
  mvprintw(16,15,"ys= %8.1f", sats->x[1]*R);
  mvprintw(16,29,"zs= %8.1f", sats->x[2]*R);
  mvprintw(17,1,"xo= %8.1f", xo);
  mvprintw(17,15,"yo= %8.1f", yo);
  mvprintw(17,29,"zo= %8.1f", zo);
  mvprintw(18,1,"vx= %8.3f", vx);
  mvprintw(18,15,"vy= %8.3f", vy);
  mvprintw(18,29,"vz= %8.3f", vz);
  mvprintw(19,0,"v0x= %8.3f", oe*xo);
  mvprintw(19,14,"v0y= %8.3f", oe*yo);
  mvprintw(19,28,"v0z= %8.3f", 0.);
#endif

}

/* TODO: rewrite, there are too many operation repeted */
void sub_sat_point(double x, double y, double z, double t,
	 double *ssplat, double *ssplon, double *height){

  double ilat, l, c;
  const double f=3.3528107e-3, e2=2*f-f*f, EPS=1e-6;

  *ssplon=atan2(y, x)-tetha(t, 0);
  while(*ssplon>M_PI)
    *ssplon-=2*M_PI;
  while(*ssplon<-M_PI)
    *ssplon+=2*M_PI;
  ilat=atan(z/sqrt(x*x+y*y));

  do{
    l=ilat;
    c=1/sqrt(1+e2*sin(l)*sin(l));
    ilat=atan((z-R*c*e2*sin(l))/sqrt(x*x+y*y));
  }while(fabs(l-ilat)>EPS);
  
  ilat=(ilat+l)/2; 
  while(ilat>M_PI/2.)
    ilat-=M_PI;
  while(ilat<-M_PI/2.)
    ilat+=M_PI;
 
  *ssplat=ilat;
  *height=sqrt(x*x+y*y)/cos(ilat)-R*c;
}


/* Get the 3 minimum value of 4 given, usefull in bracketing minimum */
void min3of4(double *a, double *fa, double *b, double *fb, 
	     double *c, double *fc, double d, double fd) {

  double v[4], t, x[4];
  int e=1, i;
 
  v[0]=*fa; v[1]=*fb; v[2]=*fc; v[3]=fd;
  x[0]=*a; x[1]=*b; x[2]=*c; x[3]=d;

  while(e){
    e=0;
    for(i=0; i<3; i++) 
      if(v[i]<v[i+1]) { 
	e=1;
	t=v[i];
	v[i]=v[i+1];
	v[i+1]=t;
	t=x[i];
	x[i]=x[i+1];
	x[i+1]=t; 
      }
  }

  e=1;
  while(e){
    e=0;
    for(i=0; i<2; i++) 
      if(x[i]<x[i+1]) { 
       e=1;
       t=v[i];
       v[i]=v[i+1];
       v[i+1]=t;
       t=x[i];
       x[i]=x[i+1];
       x[i+1]=t; 
     }
  }

  *fa=v[2];
  *fb=v[1];
  *fc=v[0];
  *a=x[2];
  *b=x[1];
  *c=x[0];
}
